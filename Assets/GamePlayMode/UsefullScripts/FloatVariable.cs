﻿
using UnityEngine;

[CreateAssetMenu(menuName = "EditorObjects/FloatVariable")]
public class FloatVariable : ScriptableObject {

    public float Value;
}
