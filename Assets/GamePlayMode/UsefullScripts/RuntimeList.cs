﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RuntimeList<T> : ScriptableObject {

    public List<T> items;
    public int Count { get { return items.Count; } }
    public void Add(T t)
    {
        if(!items.Contains(t)) items.Add(t);
    }

    public void Remove(T t)
    {
        if (!items.Contains(t)) items.Remove(t);
    }

    public bool Contains(T t)
    {
        return items.Contains(t);
    }

}
