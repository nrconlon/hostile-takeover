﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GamePlayMode : MonoBehaviour
{

    public MapHolder currentMap;
    public MapController mapController;
    [SerializeField] GameObject theSource;
    List<Vector3> spawnLocations;
    [SerializeField] GameObject mainCameraObject;
    [SerializeField] float navMeshHeight = 2;

    public Options playerOptions;

    public Camera miniMap;
    Camera mainCamera;
    [SerializeField] GameObject itemPrefab;
    [SerializeField] BuildableItem theSeed;

    public SelectPreferences giveItemPreferences;

    CancelAbility cancelAbility;

    public static GamePlayMode Instance { get; private set; }


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            cancelAbility = GetComponent<CancelAbility>();

        }
    }


    // Use this for initialization
    void Start()
    {
        mainCamera = mainCameraObject.GetComponent<Camera>();
        LoadMap(currentMap.myMap);



    }


    public void LoadMap(MapInfo map)
    {
        mapController.GenerateMap(map);
        spawnLocations = mapController.GetSpawnLocations();
        SpawnSource();
        SetUpCameras();

    }


    private void SpawnSource()
    {
        if (theSource)
        {
            SelectableObject selectableObject = SpawnSelectable(theSource, GetRandomLocation(spawnLocations));

            Vector3 startingPosition = selectableObject.transform.position;
            mainCamera.transform.position = new Vector3(startingPosition.x, 7, startingPosition.z);
            InventoryItem theSeedClone = Instantiate(theSeed);
            theSeedClone.Initialize();
            ((Unit)selectableObject).RecieveItem(theSeedClone);
            PlayerInputHandler.Instance.SelectSingle(selectableObject);

        }
    }

    private void SetUpCameras()
    {
        int sizeH = (Ship.Instance.shipHeight / 2) + 1;
        int sizeW = (Ship.Instance.shipWidth / 5) + 1;
        if (sizeH >= sizeW)
        {
            miniMap.orthographicSize = sizeH;
        }
        else
        {
            miniMap.orthographicSize = sizeW;
        }
        miniMap.transform.position = new Vector3((Ship.Instance.shipWidth - 1) / 2f, miniMap.transform.position.y, (Ship.Instance.shipHeight - 1) / 2f);
    }



    private Vector3 GetRandomLocation(List<Vector3> listOfLocations)
    {
        if (listOfLocations != null && listOfLocations.Count < 1)
        {
            return Vector3.zero;
        }
        int num = Random.Range(0, listOfLocations.Count);  //max is exclusive
        return listOfLocations[num];
    }

    public SelectableObject SpawnSelectable(GameObject spawnObject, Vector3 location)
    {
        location = new Vector3(location.x, navMeshHeight, location.z);
        GameObject spawnObjectClone = Instantiate(spawnObject, location, Quaternion.identity);
        SelectableObject selectableSpawn = spawnObjectClone.GetComponent<SelectableObject>();
        return selectableSpawn;
    }


    public Building BuildOnChunk(GameObject buildingGameObject, Chunk chunk)
    {
        Building building = buildingGameObject.GetComponent<Building>();
        if (building && chunk && buildingGameObject && chunk.MyBuilding == null)
        {
            Vector3 location = chunk.transform.position;
            location = new Vector3(location.x, navMeshHeight, location.z);
            building = Instantiate(buildingGameObject, location, Quaternion.identity).GetComponent<Building>();
            chunk.MyBuilding = building;
            building.Activate(chunk);
            return building;
        }
        else
        {
            return null;
        }
    }

    public void StartCorruption(Chunk chunk)
    {
        mapController.SeedWasPlaced(chunk.MyCoords.Column, chunk.MyCoords.Row);
    }

    public void DropItem(Vector3 position, InventoryItem item)
    {
        if (item)
        {
            position = new Vector3(position.x, navMeshHeight, position.z);
            FloorItem floorItem = Instantiate(itemPrefab, position, Quaternion.identity).GetComponent<FloorItem>();
            floorItem.AssignItem(item);
        }
    }

    public void CreateItem(Vector3 position, InventoryItem item)
    {
        if (item)
        {
            position = new Vector3(position.x, navMeshHeight, position.z);
            FloorItem floorItem = Instantiate(itemPrefab, position, Quaternion.identity).GetComponent<FloorItem>();
            InventoryItem itemClone = Instantiate(item);
            itemClone.Initialize();
            floorItem.AssignItem(itemClone);
        }
    }

    public void UpdateAbilitiesAndInfoPanels()
    {
        PlayerInputHandler.Instance.UpdateAbilitiesAndInfoPanels();
    }

    public void SpawnFloatingText(string text, Transform parent, TextColor textColor, bool upAnimation)
    {

        GameObject floatingTextObject = ObjectPooler.Instance.SpawnFromPool("FloatingText", parent);
        FloatingText floatingText = floatingTextObject.GetComponent<FloatingText>();
        floatingText.Activate(text, textColor, upAnimation);
    }


    public void AddCancelAbility(UnityEvent cancelEvent, SelectableObject cancelUser)
    {
        CancelAbility newCancelAbility = cancelUser.gameObject.AddComponent<CancelAbility>();

        newCancelAbility.name = cancelAbility.name;
        newCancelAbility.sprite = cancelAbility.sprite;
        newCancelAbility.importanceRating = cancelAbility.importanceRating;
        newCancelAbility.description = cancelAbility.description;
        newCancelAbility.SetCancel(cancelEvent);
        cancelUser.RefreshAbilities();
    }

}




