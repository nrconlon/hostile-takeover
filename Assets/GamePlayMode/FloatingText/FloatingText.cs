﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FloatingText : MonoBehaviour {

    public float DestroyTime = 3.0f; // How long is the countdown alive.
    [SerializeField] Color CorruptionTextColor;
    [SerializeField] Color HealthTextColor;
    [SerializeField] Color BioFuelTextColor;
    [SerializeField] Color DamageTextColor;
    [SerializeField] Color EnergyTextColor;
    [SerializeField] AnimationClip upAnimation;
    [SerializeField] AnimationClip downAnimation;

    private TextMesh textMesh;
    private Animation animationRef;
    private RectTransform rectTransformRef;

    private void Awake()
    {
        textMesh = GetComponent<TextMesh>();
        animationRef = GetComponent<Animation>();
        rectTransformRef = GetComponent<RectTransform>();

    }

    public void Activate(string text, TextColor textColor,bool isUpAnimation)
    {
        Invoke("DeactiveMe", DestroyTime);
        //rectTransformRef.position = new Vector3(transform.position.x, 3, transform.position.z + 0.3f);
        //rectTransformRef.SetPositionAndRotation(new Vector3(50, 3, 50 + 0.3f), Quaternion.identity);
        //transform.position =  
        textMesh.color = GetColor(textColor);
        textMesh.text = text;

        if (isUpAnimation)
        {
            animationRef.Play("floatingTextUp");
        }
        else
        {
            animationRef.Play("floatingTextDown");
        }
    }

    private void DeactiveMe()
    {
        ObjectPooler.Instance.DeactivateMe(gameObject);

    }




    private Color GetColor(TextColor textColor)
    {
        switch (textColor)
        {
            case TextColor.Corruption:
                return CorruptionTextColor;

            case TextColor.Damage:
                return DamageTextColor;

            case TextColor.Health:
                return HealthTextColor;

            case TextColor.BioFuel:
                return BioFuelTextColor;

            case TextColor.Energy:
                return EnergyTextColor;

            default:
                break;
        }
        return Color.black;
    }

}

public enum TextColor { Corruption, Damage, Health, BioFuel, Energy }
