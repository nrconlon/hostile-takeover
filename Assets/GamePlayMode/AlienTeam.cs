﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienTeam : MonoBehaviour{
    //Lists of each unit type.
    [HideInInspector] public int income = 0;
    [HideInInspector] public int upkeep = 0;
    [HideInInspector] public int currentEnergy = 0;
    [HideInInspector] public int currentBiofuel = 0;

    [HideInInspector] public SelectableObject theSource;
    [HideInInspector] public List<Unit> allWorkers = new List<Unit>();
    [HideInInspector] public Dictionary<string,List<Unit>> allMyUnits;
    [HideInInspector] public List<Building> allMyBuildings;
    [HideInInspector] public List<PlaceRoomAbility> placeRoomAbilities = new List<PlaceRoomAbility>();
    [HideInInspector] public List<SpawnUnitAbility> spawnUnitAbilities = new List<SpawnUnitAbility>();
    [HideInInspector] public List<AlienRoom> myRooms = new List<AlienRoom>();
    public ChunkPreferences buildRoomPreferences;
    public EvolveSkillList currentEvolveSkills;


    public static AlienTeam Instance { get; private set; }

    public void SetSource(SelectableObject newSource)
    {
        theSource = newSource;
    }



    private void Awake()
    {
        Instance = this;
        allMyUnits = new Dictionary<string, List<Unit>>();
        PlaceRoomAbility[] placeRoomAbilitiesArray = GetComponents<PlaceRoomAbility>();
        foreach(PlaceRoomAbility roomAbility in placeRoomAbilitiesArray)
        {
            if (IsAllowedToUse(roomAbility.dependancy))
            {
                placeRoomAbilities.Add(roomAbility);
            }
        }
        placeRoomAbilities.Sort();
    }

    public bool IsAllowedToUse(EvolveSkill dependency)
    {
        if (!dependency)
        {
            return true;
        }
        else if (currentEvolveSkills.Count > 0 && currentEvolveSkills.Contains(dependency))
        {
            return true;
        }
        return false;
    }

    internal void SpawnUnit(GameObject unit, Vector3 location)
    {
        GamePlayMode.Instance.SpawnSelectable(unit, location);
    }

    public void TickIncome(float totalCorruption)
    {
        income = (int)totalCorruption;
        if(income == 0 && totalCorruption > 0)
        {
            income = 1;
        }

        if (!AttemptApplyEnergy(income - upkeep))
        {
            //Start killing people off.
            currentEnergy = 0;
        }
    }

    public void AddSelectable(SelectableObject selectable)
    {
        if (selectable is Unit)
        {
            Unit unit = (Unit)selectable;
            if (unit.selectableType == SpecialSelectableType.Worker)
            {
                allWorkers.Add((Unit)selectable);
            }
            else if (unit.selectableType == SpecialSelectableType.TheSource)
            {
                SetSource(selectable);
            }
            else
            {
                if (allMyUnits.ContainsKey(unit.name))
                {
                    allMyUnits[unit.name].Add(unit);
                }
                else
                {
                    allMyUnits.Add(unit.name, new List<Unit> { unit }); ;
                }
                allMyUnits[unit.name].Sort();
            }

            upkeep += unit.upkeepCost;

        }
        else if (selectable is Building)
        {
            Building building = (Building)selectable;
            allMyBuildings.Add(building);
            if (building.buildingType == BuildingType.TheSeed)
            {
                GamePlayMode.Instance.StartCorruption(building.GetChunk());
            }
            foreach (Ability ability in building.GetAllAbilities())
            {
                if (ability is SpawnUnitAbility)
                {
                    spawnUnitAbilities.Add((SpawnUnitAbility)ability);
                }
            }

        }
        if (selectable.selectableType == SpecialSelectableType.TheSource)
        {
            SetSource(selectable);
        }
        selectable.DiedEvent.AddListener(RemoveSelectable);
        selectable.transform.SetParent(transform);
        CanvasController.Instance.UpdateHivePanels();
    }

    public void RemoveSelectable(SelectableObject selectable)
    {
       // Do nothing for now
    }

    public bool AttemptApplyEnergy(int energy)
    {

        if (currentEnergy + energy < 0)
        {
            return false;

        }
        else
        {
            currentEnergy += energy;
        }
        return true;
    }


    public bool AttemptApplyBioFuel(int bioFuel)
    {

        if (currentBiofuel + bioFuel < 0)
        {
            return false;

        }
        else
        {
            currentBiofuel += bioFuel;
        }
        return true;
    }

    public void BuildRoom(ChunkType roomType, Chunk chunk)
    {
        chunk.SetChunkType(roomType);
        chunk.AdjustColor(Color.white);
        AlienRoom room = GetNeighboringRoom(chunk);
        if(room == null)
        {
            room = new AlienRoom(roomType);
            myRooms.Add(room);
        }
        room.AddChunk(chunk);
        room.RefreshBuildingSlots();

    }

    AlienRoom GetNeighboringRoom(Chunk chunk)
    {
        int column = chunk.MyCoords.Column;
        int row = chunk.MyCoords.Row;
        AlienRoom room = GetAlienRoom(MapController.Instance.GetChunk(column + 1, row));
        if (room != null) return room;
        room = GetAlienRoom(MapController.Instance.GetChunk(column, row + 1));
        if (room != null) return room;
        room = GetAlienRoom(MapController.Instance.GetChunk(column - 1, row));
        if (room != null) return room;
        room = GetAlienRoom(MapController.Instance.GetChunk(column, row - 1));
        if (room != null) return room;
        return null;
    }
    AlienRoom GetAlienRoom(Chunk chunk)
    {
        if (chunk && chunk.MyCollection != null && chunk.MyCollection is AlienRoom)
        {
            AlienRoom room = (AlienRoom)chunk.MyCollection;
            if(room != null && myRooms.Contains(room))
            {
                return room;
            }
        }
        return null;
    }


    public void AddCollection(Collection collection)
    {

        ChunkType chunkType = collection.GetChunkType();
        //foreach (Ability ability in collection.GetAbilities())
        //{
        //    if (AbilityCollectionLookup.ContainsKey(ability))
        //    {
        //        AbilityCollectionLookup[ability].Add(collection);
        //    }
        //    else
        //    {
        //        List<Collection> chunkList = new List<Collection>
        //        {
        //            collection
        //        };
        //        AbilityCollectionLookup.Add(ability, chunkList);
        //    }
        //}


        //if (ChunkTypeCollectionLookup.ContainsKey(chunkType))
        //{
        //    ChunkTypeCollectionLookup[chunkType].Add(collection);
        //}
        //else
        //{
        //    List<Collection> chunkList = new List<Collection>
        //    {
        //        collection
        //    };
        //    ChunkTypeCollectionLookup.Add(chunkType, chunkList);
        //}
    }



}
