﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AlienAbility : Ability {

    protected SelectableObject myUser;

    public float aBaseCoolDown = 0f;

    protected override void Awake()
    {
        base.Awake();
        myUser = GetComponent<SelectableObject>();
    }

    public override bool IsUsable()
    {
        //if cooldown timeStamp = Time.time + coolDownPeriodInSeconds; on use
        //if (timeStamp <= Time.time)
        //{
        //    // Your firing code
        //}

        if (!base.IsUsable())
        {
            return false;
        }


        if (EnergyCost > 0)
        {
            if (AlienTeam.Instance.currentEnergy < EnergyCost)
            {
                return false;
            }
        }

        if (BioFuelCost > 0)
        {
            if (AlienTeam.Instance.currentBiofuel < BioFuelCost)
            {
                return false;
            }
        }
        return true;
    }
}
