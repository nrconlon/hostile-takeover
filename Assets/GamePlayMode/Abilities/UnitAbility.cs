﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitAbility : Ability {

    protected Unit myUnit;

    public bool isMainAttack = false;
    public float aBaseCoolDown = 1f;

    protected override void Awake()
    {
        base.Awake();

        myUnit = GetComponent<Unit>();

    }

    public override bool IsUsable()
    {
        //if cooldown
        if (!base.IsUsable())
        {
            return false;
        }

        if (BioFuelCost > 0)
        {
            if (myUnit.currentBioFuel < BioFuelCost)
            {
                return false;
            }
        }
        return true;
    }
}
