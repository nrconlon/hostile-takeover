﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;




public abstract class Ability : MonoBehaviour, IComparable<Ability>
{
    public Sprite sprite;
    public new string name = "New Ability";

    public float importanceRating = 100;
    public string description = "Description of what this ability does";

    public EvolveSkill dependancy;

    public float BioFuelCost = 0;
    public float EnergyCost = 0;


    protected virtual void Awake()
    {
        
    }




    public abstract void Activate();
    public virtual bool IsUsable()
    {
        if (AlienTeam.Instance.IsAllowedToUse(dependancy))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public int CompareTo(Ability other)
    {
        if (importanceRating < other.importanceRating)
        {
            return -1;
        }
        else if (importanceRating == other.importanceRating)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}

[Serializable]
public class ChunkPreferences
{
    public bool hasBuilding = false;
    public bool nonBuildable = false;
    public float MoreOrEqualCorruptedThan = 0;
    public bool uncorrupted = false;
    public int minimumFloorTier = 0;
    public string failMessage = "Can't build there";
    public bool FitsPreferences(Chunk chunk)
    {
        if (!hasBuilding && chunk.MyBuilding) return false;
        if (!nonBuildable && chunk.GetChunkType().name != "Floor") return false;
        if (!chunk.IsMoreCorruptedThanOrEqual(MoreOrEqualCorruptedThan)) return false;
        if (uncorrupted && chunk.IsCorrupted()) return false;
        if (chunk.floorTier < minimumFloorTier) return false;
        return true;
    }
}


[Serializable]
public class SelectPreferences
{
    public bool friendlySelectable = false;
    public bool enemySelectable = false;
    public bool enemyNeutral = false;
    public bool mustBeDead = false;
    public bool building = false;
    public bool unit = false;
    public bool mustBeHuman = false;
    public bool item = false;

    public string failMessage = "Target invalid";
    public bool FitsPreferences(SelectableObject selectableObject)
    {
        TeamStatus selectableTeam = selectableObject.GetTeam();
        if (!friendlySelectable && selectableTeam == TeamStatus.Alien) return false;
        if (!enemySelectable && selectableTeam == TeamStatus.Human) return false;
        if (!enemyNeutral && selectableTeam == TeamStatus.Neutral || selectableTeam == TeamStatus.None) return false;
        if (!building && (selectableObject is Building)) return false;
        if (!item && (selectableObject is FloorItem)) return false;
        if (!unit && (selectableObject is Unit)) return false;
        if (mustBeDead && !selectableObject.IsDead()) return false;
        if (mustBeHuman && !selectableObject.IsHuman()) return false;
        return true;
    }
}

[Serializable]
public class Attack
{
    public float damage;
    public float corruptionApply;
    public float piercing;
    public float suspcionThroughDamage;
    public bool headAttack = false;



}

