﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildAbility : UnitAbility {

    public GameObject buildingObject;
    public ChunkPreferences preferences;
    private Chunk targetChunk;
    public bool freeBuild = false;
    public bool useFester = false;
    public Fester fester;


    public override void Activate()
    {
        if (EnergyCost > 0)
        {

        }
        if (BioFuelCost > 0 && myUnit.currentBioFuel < BioFuelCost)
        {
            Debug.Log("Not Enough BioFuel");
            return;
        }
        EventChunk eventChunk = new EventChunk();
        eventChunk.AddListener(Use);
        Building building = buildingObject.GetComponent<Building>();
        PlayerInputHandler.Instance.AbilityChunk(eventChunk, preferences, building.GetSprite());
    }

    public void Use(Chunk chunk)
    {
        targetChunk = chunk;
        Vector3 position = chunk.transform.position;
        if (myUnit)
        {
            SpriteRenderer buildingSprite = PlayerInputHandler.Instance.GetBuildingSpriteRenderer();
            buildingSprite.sprite = buildingObject.GetComponent<Building>().GetSprite();
            EventFloor build = new EventFloor();
            build.AddListener(Build);
            myUnit.MoveAndBuildOnChunk(build, position, buildingSprite.gameObject, 0.7f);

        }
        else
        {
            Build(position);
        }


    }

    public void Build(Vector3 position)
    {
        if (BioFuelCost > 0)
        {
            if (myUnit.currentBioFuel < BioFuelCost)
            {
                Debug.Log("Not Enough BioFuel");
                return;
            }
            else
            {
                myUnit.AddBioFuel(-BioFuelCost);
            }
        }
        Building building = myUnit.BuildOnChunk(buildingObject, targetChunk);
        if(useFester)
        {
            fester.SetIsBuilding(true);
            building.FesterBody(myUnit, fester);
        }
    }
}
