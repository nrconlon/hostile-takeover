﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceRoomAbility : Ability {

    public ChunkTypeCollection chunkType;
    private Chunk targetChunk;


    public override void Activate()
    {
        if (IsUsable())
        {
            EventChunk eventChunk = new EventChunk();
            eventChunk.AddListener(Use);
            PlayerInputHandler.Instance.AbilityChunk(eventChunk, AlienTeam.Instance.buildRoomPreferences, chunkType.sprite,InputMode.Persist);
        }
        else
        {
            Debug.Log("Not Enough Recourses");
        }

    }

    public void Use(Chunk chunk)
    {
        Destroy(PlayerInputHandler.Instance.GetBuildingSpriteRenderer().gameObject);
        if (IsUsable())
        {

            AlienTeam.Instance.BuildRoom(chunkType, chunk);
        }
        else
        {
            Debug.Log("Not Enough Recourses");
        }


    }

    public override bool IsUsable()
    {
        if (!base.IsUsable())
        {
            return false;
        }
        if (EnergyCost > 0)
        {
            if (AlienTeam.Instance.currentEnergy < EnergyCost)
            {
                return false;
            }
        }

        if (BioFuelCost > 0)
        {
            if (AlienTeam.Instance.currentBiofuel < BioFuelCost)
            {
                return false;
            }
        }

        return true;
    }
}
