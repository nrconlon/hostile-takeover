﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FesterBody : SingleTarget {
    public Fester fester;

    public override void Perform(SelectableObject target)
    {
        target.FesterBody(myUnit, fester);
    }

}
