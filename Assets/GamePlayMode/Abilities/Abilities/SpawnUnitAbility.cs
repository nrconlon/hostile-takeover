﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnUnitAbility : AlienAbility {
    public GameObject unit;
    public float timeToSpawn;
    public float timeBetweenTicks = 0.2f;
    private Coroutine channelingCoroutine;

    public override void Activate()
    {
        if(IsUsable())
        {
            //turn on spawning unit animations
            channelingCoroutine = StartCoroutine(SpawnUnitTimer());
            //AlienTeam.Instance.
        }
        else
        {
            Debug.Log("Not enough recourses");
        }
    }

    void StopChanneling()
    {
        //If spawning unit animations, turn off spawning unit animations
        if (channelingCoroutine != null)
        {
            //Called from coroutine, so don't come in here if from there
            StopCoroutine(channelingCoroutine);
            channelingCoroutine = null;
        }


    }

    IEnumerator SpawnUnitTimer()
    {
        float tickCount = 0;
        while (tickCount < timeToSpawn)
        {
            //update building spawning unit timer
            tickCount += timeBetweenTicks;
            yield return new WaitForSeconds(timeBetweenTicks);
        }
        channelingCoroutine = null;
        StopChanneling();
        AlienTeam.Instance.SpawnUnit(unit,myUser.transform.position);
    }


    public override bool IsUsable()
    {
        return base.IsUsable();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
