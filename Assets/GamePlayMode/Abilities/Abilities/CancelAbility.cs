﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CancelAbility : Ability {

    UnityEvent cancelEvent;

    public override void Activate()
    {
        if (cancelEvent != null)
        {
            cancelEvent.Invoke();
        }

    }

    public void SetCancel(UnityEvent cancelEvent)
    {
        this.cancelEvent = cancelEvent;
    }


}
