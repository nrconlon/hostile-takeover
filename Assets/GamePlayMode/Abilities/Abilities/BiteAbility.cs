﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BiteAbility : SingleTarget {


    private Coroutine channelingCoroutine = null;


    public float bioFuelPerTick;
    public float timeBetweenTick;
    public float totalTicks;







    public override void Perform(SelectableObject target)
    {
        //walk up and bite his bitch ass.
        //Add to a coroutine that can be canceled if a new action is given.
        UnityEvent cancelChannelingEvent = new UnityEvent();
        cancelChannelingEvent.AddListener(StopChanneling);
        channelingCoroutine = StartCoroutine(ChannelingTick(target));
        myUnit.StartedChanneling(cancelChannelingEvent);
    }

    
 



    void StopChanneling()
    {
        if (channelingCoroutine != null)
        {
            StopCoroutine(channelingCoroutine);
        }
        channelingCoroutine = null;

    }


    IEnumerator ChannelingTick(SelectableObject target)
    {
        float tickCount = 0;
        while(tickCount < totalTicks)
        {
            Unit unit = (Unit)target;
            if (unit)
            {
                //unit.ApplySuspicion(suspicionApply);
                if (unit.ApplyAttack(damage))
                {
                    myUnit.AddBioFuel(bioFuelPerTick);
                }
                else
                {
                    myUnit.StopChanneling();
                }

            }
            tickCount++;
            yield return new WaitForSeconds(timeBetweenTick);
        }

    }

}
