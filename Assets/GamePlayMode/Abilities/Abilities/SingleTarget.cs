﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleTarget : UnitAbility {

    public SelectPreferences preferences;
    public Attack damage;


    protected override void Awake()
    {
        base.Awake();
        if (isMainAttack)
        {
            EventSelectable eventSelectable = new EventSelectable();
            eventSelectable.AddListener(Use);
            myUnit.SetMainAttack(eventSelectable, preferences);
        }
    }

    public override void Activate()
    {
        EventSelectable eventSelectable = new EventSelectable();
        eventSelectable.AddListener(Use);
        PlayerInputHandler.Instance.AbilitySelectable(eventSelectable, preferences);

    }

    public virtual void Use(SelectableObject target)
    {
        EventSelectable eventSelectable = new EventSelectable();
        eventSelectable.AddListener(Perform);
        myUnit.MoveAndPerform(target, eventSelectable);
    }

    public virtual void Perform(SelectableObject target)
    {
        target.ApplyAttack(damage);
    }


}
