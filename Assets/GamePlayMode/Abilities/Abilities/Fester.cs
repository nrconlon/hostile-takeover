﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class Fester  {
    //The fester holds the data for the target to use.  The target runs the coroutines
    public GameObject newCreation;
    public Sprite festeringSprite;
    public float timeBetweenTick;
    public float totalTicks;

    [HideInInspector] public Unit festerer;
    [HideInInspector] public Sprite oldSprite;
    bool isBuilding = false;



    public void StartFester(Unit festererUnit, Sprite oldSprite, SelectableObject target)
    {
        target.SetTeam(TeamStatus.Alien);
        this.oldSprite = oldSprite;
        festerer = festererUnit;
        if (SelectableObject.currentlySelected.Contains(festererUnit))
        {
            PlayerInputHandler.Instance.SelectSingleAdd(target);
        }

        festererUnit.KillAndRemove();
        if(festeringSprite)
        {
            target.SetSprite(festeringSprite);
        }
        UnityEvent cancelFesterEvent = new UnityEvent();
        cancelFesterEvent.AddListener(target.CancelFester);
        GamePlayMode.Instance.AddCancelAbility(cancelFesterEvent, target);
    }

    public void SetIsBuilding(bool isBuilding)
    {
        this.isBuilding = isBuilding;
    }

    public void CancelFester(SelectableObject target)
    {
        festerer.gameObject.SetActive(true);
        festerer.ReturnToLife();
        if (isBuilding)
        {
            if (SelectableObject.currentlySelected.Contains(target))
            {
                PlayerInputHandler.Instance.SelectSingleAdd(festerer);
            }
            target.KillAndDelete();
        }
        else
        {
            target.ResetTeam();
            target.SetSprite(oldSprite);
        }


    }



    public void ConsumeBody(SelectableObject deadBody)
    {
        //Create new source human.
        SelectableObject newUnit = GamePlayMode.Instance.SpawnSelectable(newCreation, deadBody.transform.position);
        //TODO If Human, Take surface data (Names, looks, persona, opinions) and apply to new human
    }


}
