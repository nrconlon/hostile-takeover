﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship :  Singleton<Ship>
{
    Dictionary<ChunkType, List<Collection>> ChunkTypeCollectionLookup = new Dictionary<ChunkType, List<Collection>>();
    List<Collection> allMyCollections = new List<Collection>();

    Dictionary<Ability, List<Collection>> AbilityCollectionLookup = new Dictionary<Ability, List<Collection>>();

    List<Building> myDoors = new List<Building>();

    [HideInInspector] public int shipHeight = 0;
    [HideInInspector] public int shipWidth = 0;

    public GameObject baseHumanPrefab;
    public Transform chunkHolder;


    private float maxEnergy = 100;
    private float currentEnergy = 0;
    private bool IsOn = false;


    private void Start()
    {
    }


    public void PowerOn()
    {
        IsOn = true;
    }

    public void PowerOff()
    {
        IsOn = false;
    }

    public void AddSelectable(SelectableObject selectable)
    {
        if (selectable is Unit)
        {
            Unit unit = (Unit)selectable;
        }
        else if (selectable is Building)
        {
            Building building = (Building)selectable;
            if(building is Door)
            {
                myDoors.Add((Door)building);
            }

        }
        selectable.DiedEvent.AddListener(RemoveSelectable);
        selectable.transform.SetParent(transform);
        CanvasController.Instance.UpdateHivePanels();
    }

    public void RemoveSelectable(SelectableObject selectable)
    {
        // Do nothing for now
    }

    public void AddCollectionToShip(Collection collection)
    {

        ChunkType chunkType = collection.GetChunkType();
        //foreach (Ability ability in collection.GetAbilities())
        //{
        //    if (AbilityCollectionLookup.ContainsKey(ability))
        //    {
        //        AbilityCollectionLookup[ability].Add(collection);
        //    }
        //    else
        //    {
        //        List<Collection> chunkList = new List<Collection>
        //        {
        //            collection
        //        };
        //        AbilityCollectionLookup.Add(ability, chunkList);
        //    }
        //}

        if (!chunkType) return;
        if (ChunkTypeCollectionLookup.ContainsKey(chunkType))
        {
            if (!ChunkTypeCollectionLookup[chunkType].Contains(collection))
            {
                ChunkTypeCollectionLookup[chunkType].Add(collection);
            }

        }
        else
        {
            List<Collection> chunkList = new List<Collection>
            {
                collection
            };
            ChunkTypeCollectionLookup.Add(chunkType, chunkList);
        }
    }

    public List<Collection> GetCollectionsOfType(ChunkType chunkType)
    {
        return ChunkTypeCollectionLookup[chunkType];
    }

    private List<Collection> GetFurnitureCollections()
    {
        List<Collection> furnitureList = new List<Collection>();

        foreach (ChunkType chunkType in ChunkTypeCollectionLookup.Keys)
        {
            if (chunkType is ChunkTypeFurniture)
            {
                foreach (Collection collection in ChunkTypeCollectionLookup[chunkType])
                {
                    furnitureList.Add(collection);

                }
            }
        }

        return furnitureList;
    }

    private List<Collection> GetShipPieceCollections()
    {
        List<Collection> shipPieceList = new List<Collection>();

        foreach (ChunkType chunkType in ChunkTypeCollectionLookup.Keys)
        {
            if (chunkType is ChunkTypeCollection)
            {
                foreach (Collection collection in ChunkTypeCollectionLookup[chunkType])
                {
                    shipPieceList.Add(collection);

                }
            }
        }

        return shipPieceList;
    }

    public void GenerateHumans(MapInfo map)
    {
        //Create humans, Decide personallities, spawn them where they need to be.
        //Assign each of them the task of using the task that they were spawned to do. 
        int numberOfHumans = Random.Range(map.minHumans, map.maxHumans + 1);
        foreach(Collection collection in GetShipPieceCollections())
        {
            if (map.dontSpawnHumansInCollection == collection.GetChunkType()) continue;
            if(numberOfHumans > 0)
            {
                //collection.GetChunkType().HumanTraits
                Chunk Interactable = collection.GetInteractChunk();
                Chunk nearestFloor = MapController.Instance.GetNearestFloor(Interactable);
                if (nearestFloor)
                {
                    Unit human = (Unit)GamePlayMode.Instance.SpawnSelectable(baseHumanPrefab, nearestFloor.transform.position);
                    //pilot.assign(humanStats);
                    //pilot.assignTask(Interactable);
                    numberOfHumans--;
                }
            }
            else
            {
                return;
            }

        }

        List<Collection> furnitureCollections = GetFurnitureCollections();

        for (int i = 0; i < numberOfHumans; i++)
        {
            if (furnitureCollections.Count < 1) return;
            Collection collection = furnitureCollections[Random.Range(0, furnitureCollections.Count)];
            furnitureCollections.Remove(collection);


            //collection.GetChunkType().HumanTraits
            Chunk Interactable = collection.GetInteractChunk();
            Chunk nearestFloor = MapController.Instance.GetNearestFloor(Interactable);
            if (nearestFloor)
            {
                Unit human = (Unit)GamePlayMode.Instance.SpawnSelectable(baseHumanPrefab, nearestFloor.transform.position);
                //pilot.assign(humanStats);
                //pilot.assignTask(Interactable);
            }
            else
            {
                i--;
            }
        }      
    }

}



enum ShipType
{
    Traveling,
    Pleasure,
    Living,
    Mining,
    Cargo,
    Military,
    Research,
    Religious,
    Pirate,
    Illegal,
    Prisoner,
};