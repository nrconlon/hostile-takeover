﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using UnityEngine;


public class MapController : MonoBehaviour
{

    private GridSquare[,] gridSquares;
    private Chunk[,] gridChunks;


    public ListOfChunkTypes allChunkTypes;
    public Color corruptionColorBase;
    public float corruptionColorAlpha = 0.7f;

    private HashSet<Chunk> allChunks = new HashSet<Chunk>();


    private HashSet<GridSquare> spreadCorruptionGridSet;
    private HashSet<GridSquare> corruptGridSet;
    private HashSet<GridSquare> drainGridSet;
    public HashSet<Chunk> chunksWithBuildings = new HashSet<Chunk>();
    public HashSet<Unit> corruptableHumans = new HashSet<Unit>();
    List<Chunk> turnedOffSideChunk;

    public GameObject collectionPrefab;
    public GameObject chunkPrefab;
    public GameObject gridSquarePrefab;

    private Texture2D shipTextureLayout;

    public int chunkWidth = 5;
    [SerializeField] float secondsToCorrupt = 60f; //  DECIDES HOW QUICKLY. 
    [SerializeField] float spreadCorruptionThreshholdBase = 0.02f; // Decides how Spready
    [SerializeField] float corruptionRemoveMultiplier = -1f; // Decides much decays compared to how much it spreads.

    List<Vector3> spawnLocations;



    public GameObject DoorPrefab;
    public GameObject defaultInteractable;


    [Header("Height Types")]
    [SerializeField] float heightNormal = 0;
    [SerializeField] float heightMedium = 0.2f;
    [SerializeField] float heightHigh = .6f;
    [SerializeField] float heightMax = 1;
    [SerializeField] float heightEmpty = -5;

    [Header("Multiplier Types")]
    [SerializeField] float verySlowMultiplier = 0.1f;
    [SerializeField] float slowMultiplier = 0.3f;
    [SerializeField] float mediumMultiplier = 0.5f;
    [SerializeField] float fastMultiplier = 0.7f;


    List<Pair> seedLocations;

    Chunk foundInteractable = null;
    List<Chunk> walkableSurfaces =  new List<Chunk>();

    [SerializeField] float HowOftenCorruptionTick = 1f;
    [HideInInspector] public bool chunkHasChanged = false;


    public static MapController Instance { get; private set; }

     
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }


    public void GenerateMap(MapInfo map)
    {
        Texture2D layout = map.map;
        gridSquares = new GridSquare[layout.width * chunkWidth, layout.height * chunkWidth];
        gridChunks = new Chunk[layout.width, layout.height];

        spreadCorruptionGridSet = new HashSet<GridSquare>();
        corruptGridSet = new HashSet<GridSquare>();
        drainGridSet = new HashSet<GridSquare>();
        turnedOffSideChunk = new List<Chunk>();

        spawnLocations = new List<Vector3>();
        seedLocations = new List<Pair>();

        shipTextureLayout = layout;

        allChunkTypes.LoadAll();
        GenerateShip();
        SortChunksIntoCollections();
        ActivateNavMesh();
        Ship.Instance.PowerOn();
        Ship.Instance.GenerateHumans(map);
        CanvasController.Instance.LoadCorruptionHandlerTiles(allChunks);
        SetChunksConnected();


        StartCoroutine(CorruptionTick());
    }

    public void ClearMap()
    {
        //if (transform.childCount > 0)
        //{
        //    foreach (Transform child in transform)
        //    {
        //        Destroy(child.gameObject);
        //    }
        //}
        Array.Clear(gridSquares, 0, gridSquares.Length);
        Array.Clear(gridChunks, 0, gridChunks.Length);

        spreadCorruptionGridSet.Clear();
        corruptGridSet.Clear();
        drainGridSet.Clear();

        spawnLocations.Clear();
        walkableSurfaces.Clear();
    }

    public Chunk GetNearestFloor(Chunk interactable)
    {
        Pair coords = interactable.MyCoords;
        int column = coords.Column;
        int row = coords.Row;
        Chunk chunk1 = gridChunks[column + 1, row];
        if (chunk1 && chunk1.GetChunkType().IsFloor()) return chunk1;
        Chunk chunk2 = gridChunks[column, row + 1];
        if (chunk2 && chunk2.GetChunkType().IsFloor()) return chunk2;
        if (column > 0)
        {
            Chunk chunk3 = gridChunks[column - 1, row];
            if (chunk3 && chunk3.GetChunkType().IsFloor()) return chunk3;
        }
        if (row > 0)
        {
            Chunk chunk4 = gridChunks[column, row - 1];
            if (chunk4 && chunk4.GetChunkType().IsFloor()) return chunk4;
        }
        return null;
    }

    private bool IsNeighboringChunksCorrupted(Pair coords)
    {
        int column = coords.Column;
        int row = coords.Row;
        if (gridChunks[column + 1, row] && gridChunks[column + 1, row].IsCorrupted()) return true;
        if (gridChunks[column, row + 1] && gridChunks[column, row + 1].IsCorrupted()) return true;
        if(column > 0 && gridChunks[column - 1, row])
        {
            if (gridChunks[column - 1, row].IsCorrupted()) return true;
        }
        if (row > 0 && gridChunks[column, row - 1])
        {
            if (gridChunks[column, row - 1].IsCorrupted()) return true;
        }
        return false;
    }

    private void RefreshCorruptionTick()
    {
        spreadCorruptionGridSet.Clear();
        corruptGridSet.Clear();
        drainGridSet.Clear();



        foreach (Chunk chunk in turnedOffSideChunk)
        {
            chunk.TurnOnSides();
        }
        turnedOffSideChunk.Clear();
        foreach (Chunk chunk in allChunks)
        {
            if (chunk.IsCorruptableToStart())
            {
                //Check if there is any corruption in or around it. otherwise, nothing we can do
                if (chunk.IsCorrupted() || IsNeighboringChunksCorrupted(chunk.MyCoords))
                {
                    //For each square, corrupting, add it to the list
                    foreach (GridSquare square in chunk.myGridSquares)
                    {
                        if (square.IsCorrupted())
                        {
                            if (square.GetCorruption() < 1)
                            {
                                corruptGridSet.Add(square);
                            }

                        }
                        else
                        {
                            //if not corrupted check neighbors if they have any over threshold add them to spread list
                            Pair mycoords = square.GetCoords();
                            int column = mycoords.Column;
                            int row = mycoords.Row;
                            GridSquare grid1 = gridSquares[column + 1, row];
                            if (grid1 && grid1.CanSpread())
                            {
                                spreadCorruptionGridSet.Add(grid1);
                            }
                            GridSquare grid2 = gridSquares[column, row + 1];
                            if (grid2 && grid2.CanSpread())
                            {
                                spreadCorruptionGridSet.Add(grid2);
                            }
                            if (column > 0)
                            {
                                GridSquare grid3 = gridSquares[column - 1, row];
                                if (grid3 && grid3.CanSpread())
                                {
                                    spreadCorruptionGridSet.Add(grid3);
                                }
                            }
                            if (row > 0)
                            {
                                GridSquare grid4 = gridSquares[column, row - 1];
                                if (grid4 && grid4.CanSpread())
                                {
                                    spreadCorruptionGridSet.Add(grid4);
                                }
                            }


                        }
                    }
                }
            }
            else
            {
                if (chunk.IsCorrupted())
                {
                    foreach (GridSquare gridSquare in chunk.myGridSquares)
                    {

                        if (gridSquare.IsCorrupted())
                        {
                            drainGridSet.Add(gridSquare);
                        }
                    }
                }
            }
        }

        foreach (Chunk chunk in allChunks)
        {
            if(!chunk.IsCorruptionTurnedOn())
            {
                SetNeighborsNonSides(chunk.MyCoords);
            }

        }
    }

    private List<Chunk> SetNeighborsNonSides(Pair coords)
    {
        List<Chunk> neighbors = new List<Chunk>();
        //Setting the side chunks that are corruptable to lose their sides
        int column = coords.Column;
        int row = coords.Row;
        Chunk chunk = gridChunks[column + 1, row];
        if (chunk && chunk.IsConnectable())
        {
            TurnOffSide(chunk,Direction.WEST);
        }
        chunk = gridChunks[column, row + 1];
        if (chunk && chunk.IsConnectable())
        {
            TurnOffSide(chunk, Direction.SOUTH);
        }
        if (column > 0)
        {
            chunk = gridChunks[column - 1, row];
            if (chunk && chunk.IsConnectable()) TurnOffSide(chunk, Direction.EAST);
        }
        if (row > 0)
        {
            chunk = gridChunks[column, row - 1];
            if (chunk && chunk.IsConnectable()) TurnOffSide(chunk, Direction.NORTH);
        }
        return neighbors;
    }

    private void TurnOffSide(Chunk chunk, Direction direction)
    {
        turnedOffSideChunk.Add(chunk);
        //turn off the side from here, which sets the corruptable value, and adds it to the correct list (drain or self corrupt)
        List<GridSquare> myGridSquares = chunk.myGridSquares;
        Pair coords = chunk.MyCoords;
        switch (direction)
        {
            case Direction.WEST:
                foreach (GridSquare square in chunk.myGridSquares)
                {
                    if (square.GetCoords().Column == (coords.Column * chunkWidth))
                    {
                        square.sideTurnedOff = true;
                        //add to drain list if nessesary, remove from self corruptlist  whats the third list?
                        if(square.IsCorrupted())
                        {
                            drainGridSet.Add(square);
                        }
                        corruptGridSet.Remove(square);
                    }
                }
                break;
            case Direction.EAST:
                foreach (GridSquare square in myGridSquares)
                {
                    if (square.GetCoords().Column == (coords.Column * chunkWidth) + chunkWidth - 1)
                    {
                        square.sideTurnedOff = true;
                        if (square.IsCorrupted())
                        {
                            drainGridSet.Add(square);
                        }
                        corruptGridSet.Remove(square);
                    }
                }
                break;
            case Direction.NORTH:
                foreach (GridSquare square in myGridSquares)
                {
                    if (square.GetCoords().Row == (coords.Row * chunkWidth) + chunkWidth - 1)
                    {
                        square.sideTurnedOff = true;
                        if (square.IsCorrupted())
                        {
                            drainGridSet.Add(square);
                        }
                        corruptGridSet.Remove(square);
                    }
                }
                break;
            case Direction.SOUTH:
                foreach (GridSquare square in myGridSquares)
                {
                    if (square.GetCoords().Row == (coords.Row * chunkWidth))
                    {
                        square.sideTurnedOff = true;
                        if (square.IsCorrupted())
                        {
                            drainGridSet.Add(square);
                        }
                        corruptGridSet.Remove(square);
                    }
                }
                break;
            default:
                break;
        }
    }

    IEnumerator CorruptionTick()
    {
        while(true)
        {
            HashSet<GridSquare> spreadCorruptionGridListRemoval = new HashSet<GridSquare>();
            HashSet<GridSquare> corruptGridListRemoval = new HashSet<GridSquare>();
            HashSet<GridSquare> drainGridListRemoval = new HashSet<GridSquare>();
            if (chunkHasChanged)
            {
                RefreshCorruptionTick();
                chunkHasChanged = false;
            }


            //ATTEMPT ONE
            //For selfCorruptionGridSet, Keep it corrupting until it's fully corrupted
            foreach (GridSquare currentGridSquare in corruptGridSet)
            {
                if (currentGridSquare.IsCorruptable())
                {
                    bool canSpreadBefore = currentGridSquare.CanSpread();
                    currentGridSquare.ApplyCorruption();
                    if (!canSpreadBefore && currentGridSquare.CanSpread())
                    {
                        spreadCorruptionGridSet.Add(currentGridSquare);
                    }
                    if (currentGridSquare.GetCorruption() >= 1)
                    {
                        corruptGridListRemoval.Add(currentGridSquare);
                    }
                }
                else
                {
                    corruptGridListRemoval.Add(currentGridSquare);
                    if (currentGridSquare.IsCorrupted())
                    {
                        drainGridSet.Add(currentGridSquare);
                    }
                }
            }

            //Drain
            //Remove corruption from chunks that are turned off.
            foreach (GridSquare currentGridSquare in drainGridSet)
            {
                currentGridSquare.RemoveCorruptionTick(corruptionRemoveMultiplier);
                if (!currentGridSquare.IsCorrupted())
                {
                    drainGridListRemoval.Add(currentGridSquare);
                }
            }

            // For sideCorruptionGridSet, it is here because it passed a threashold.  Spread it to all those uncorrupted, and remove it from the list
            foreach (GridSquare currentGridSquare in spreadCorruptionGridSet)
            {
                if (currentGridSquare.CanSpread())
                {
                    //Spread and Remove
                    SpreadSquare(currentGridSquare.GetCoords());
                    spreadCorruptionGridListRemoval.Add(currentGridSquare);
                }
                else
                {
                    //False alarm, this grid can't spread.  Remove it.
                    spreadCorruptionGridListRemoval.Add(currentGridSquare);
                }
            }
            float totalCorruption = 0;
            foreach (Chunk chunk in allChunks)
            {
                totalCorruption += chunk.GetCorruption();
            }
            AlienTeam.Instance.TickIncome(totalCorruption);
            CanvasController.Instance.UpdateAlienValues();

            foreach (GridSquare square in spreadCorruptionGridListRemoval)
            {
                spreadCorruptionGridSet.Remove(square);
            }
            foreach (GridSquare square in corruptGridListRemoval)
            {
                corruptGridSet.Remove(square);
            }
            foreach (GridSquare square in drainGridListRemoval)
            {
                drainGridSet.Remove(square);
            }

            //Selectable corruption ticks
            foreach(Chunk chunk in chunksWithBuildings)
            {
                chunk.CorruptBuilding();
            }
            yield return new WaitForSeconds(HowOftenCorruptionTick);
        }
        

    }

    private void SpreadSquare(Pair coords)
    {
        int column = coords.Column;
        int row = coords.Row;
        GridSquare grid1 = gridSquares[column + 1, row];
        if (grid1 && grid1.IsCorruptable() && !grid1.IsCorrupted())
        {
            corruptGridSet.Add(grid1);
        }
        GridSquare grid2 = gridSquares[column, row + 1];
        if (grid2 && grid2.IsCorruptable() && !grid2.IsCorrupted())
        {
            corruptGridSet.Add(grid2);
        }
        if (column > 0)
        {
            GridSquare grid3 = gridSquares[column - 1, row];
            if (grid3 && grid3.IsCorruptable() && !grid3.IsCorrupted())
            {
                corruptGridSet.Add(grid3);
            }
        }
        if(row > 0)
        {
            GridSquare grid4 = gridSquares[column, row - 1];
            if (grid4 && grid4.IsCorruptable() && !grid4.IsCorrupted())
            {
                corruptGridSet.Add(grid4);
            }
        }
    }

    public void ChunkHasChanged()
    {
        chunkHasChanged = true;
        SetChunksConnected();
    }

    public Chunk GetChunk(int column, int row)
    {
        if (column < 0 || row < 0)
        {
            return null;
        }
        return gridChunks[column, row];
    }

    private void SetChunksConnected()
    {
        HashSet<Chunk> alreadyChecked = new HashSet<Chunk>();
        foreach (Chunk chunk in allChunks)
        {
            chunk.SetConnectedToSeed(false);
        }
        foreach (Pair startingPair in seedLocations)
        {
            GridSquare consideredGrid = gridSquares[startingPair.Column, startingPair.Row];
            Chunk startingChunk = consideredGrid.GetChunk();
            Pair startingCoord = startingChunk.MyCoords;
            RecursiveAddConnected(startingCoord.Column, startingCoord.Row, alreadyChecked); 

        }
        alreadyChecked.Clear();
    }

    private void RecursiveAddConnected(int column, int row, HashSet<Chunk> alreadyChecked)
    {
        if (column < 0 || row < 0) return;
        Chunk startingChunk = gridChunks[column, row];

        if (alreadyChecked.Contains(startingChunk)) return;

        alreadyChecked.Add(startingChunk);
        if (startingChunk && startingChunk.IsConnectable())
        {
            startingChunk.SetConnectedToSeed(true);
            if (startingChunk.GetCorruption() <= 0) return; //Do not check further if this block is not corrupted, as others are not connected to the seed anymore.

            RecursiveAddConnected(column + 1, row, alreadyChecked);
            RecursiveAddConnected(column, row + 1, alreadyChecked);
            RecursiveAddConnected(column - 1, row, alreadyChecked);
            RecursiveAddConnected(column, row - 1, alreadyChecked);
        }

    }

   

    public HashSet<Chunk> GetAllChunks()
    {
        return allChunks;
    }


    public void SeedWasPlaced(int column, int row)
    {
        GridSquare startingCorruption;
        if (chunkWidth % 2 == 0)
        {
            startingCorruption = gridSquares[column * chunkWidth + ((chunkWidth) / 2), row * chunkWidth + ((chunkWidth) / 2)];

        }
        else
        {
            startingCorruption = gridSquares[column * chunkWidth + ((chunkWidth - 1) / 2), row * chunkWidth + ((chunkWidth - 1) / 2)];
        }
        seedLocations.Add(startingCorruption.GetCoords());
        CanvasController.Instance.ToggleCorruptionSingle(startingCorruption.GetChunk()); // This will call a reset
        startingCorruption.ApplyCorruption();
        corruptGridSet.Add(startingCorruption);


    }

    private void ActivateNavMesh()
    {
        foreach (Chunk chunk in walkableSurfaces)
        {
            chunk.TurnOnNavMesh();
        }
        walkableSurfaces[0].BuildNavMesh();
    }


    public List<Vector3> GetSpawnLocations()
    {
        return spawnLocations;
    }

    private void GenerateShip()
    {

        int shipHeight = 0;

        int column = 0;
        int row = 0;
        //IgnoreSpace
        for (int pixelColumn = 0; pixelColumn < shipTextureLayout.width; pixelColumn++)
        {
            bool addedSomething = false;
            for (int pixelRow = 0; pixelRow < shipTextureLayout.height; pixelRow++)
            {
                Color pixelColor = shipTextureLayout.GetPixel(pixelColumn, pixelRow);
                if(pixelColor.a == 0)
                {
                    continue;
                }
                ChunkType chunkType = allChunkTypes.GetChunkTypeByColor(pixelColor);

                GenerateChunk(column, row, chunkType, pixelColor);
                addedSomething = true;
                row++;

            }
            if(row > shipHeight)
            {
                shipHeight = row;
            }
            if(addedSomething) column++;
            row = 0;
        }
        Ship.Instance.shipHeight = shipHeight;
        Ship.Instance.shipWidth = column;

    }

    /// <summary>
    /// Generates a chunk of n by n.  Then combines the meshes.
    ///
    /// </summary>
    /// <param name="column"></param>
    /// <param name="row"></param>

    void GenerateChunk(int column, int row, ChunkType chunkType, Color pixelColor)
    {
        if (chunkType == null)
        {
            chunkType = allChunkTypes.GetChunkTypeByName("NullType");
            Debug.Log("missing chunk type with color : " + pixelColor);
            chunkType.pixelColor = pixelColor;
        }
        if (chunkType.IsAlienSpawn())
        {
            spawnLocations.Add(new Vector3(column, 1, row));
            chunkType = allChunkTypes.GetFloor();
        }


        Chunk currentChunk = Instantiate(chunkPrefab, Vector3.zero, Quaternion.identity).GetComponent<Chunk>();
        currentChunk.MyCoords = new Pair(column, row);
        currentChunk.SetColors(corruptionColorBase, corruptionColorAlpha);



        Transform chunkTransform = currentChunk.transform;
        chunkTransform.SetParent(Ship.Instance.chunkHolder);


        Vector3 position;
        position.x = column;
        position.y = 0;
        position.z = row;
        chunkTransform.localPosition = position;
        currentChunk.SetChunkType(chunkType);
        gridChunks[column, row] = currentChunk;
        allChunks.Add(currentChunk);


        float transformSize = 1f / chunkWidth;
        for (int i = 0; i < chunkWidth; i++)
        {
            for (int j = 0; j < chunkWidth; j++)
            {

                GridSquare gridSquare = Instantiate(gridSquarePrefab, Vector3.zero, Quaternion.identity).GetComponent<GridSquare>();
                gridSquare.SetCorruptionInfo(secondsToCorrupt, HowOftenCorruptionTick, spreadCorruptionThreshholdBase, chunkWidth); //Needs to be set before square type
                gridSquare.SetChunk(currentChunk);




                Transform gridTransform = gridSquare.transform;
                gridTransform.SetParent(chunkTransform, false);
                if(chunkWidth % 2 == 0)
                {
                    position.x = (transformSize * (i - ((chunkWidth) / 2f))) + (0.5f/chunkWidth);
                    position.y = 0;
                    position.z = (transformSize * (j - ((chunkWidth) / 2f))) + (0.5f / chunkWidth);
                }
                else
                {
                    position.x = (transformSize * (i - ((chunkWidth - 1f) / 2f)));
                    position.y = 0;
                    position.z = (transformSize * (j - ((chunkWidth - 1f) / 2f)));
                }

                gridTransform.localPosition = position;

                gridTransform.localScale = new Vector3(transformSize, 1, transformSize);


                int currentActualRow = (row * chunkWidth) + j;
                int currentActualColumn = (column * chunkWidth) + i;

                currentChunk.AddGridSquare(gridSquare);
                gridSquare.SetCoords(new Pair(currentActualColumn, currentActualRow));  // Needs to be set before chunk type as well

                gridSquare.SetChunkType(chunkType);
                gridSquare.SetCorruptedColor(corruptionColorBase, corruptionColorAlpha);


                gridSquares[currentActualColumn, currentActualRow] = gridSquare;
            }
        }
        if(chunkType.sprite)
        {
            currentChunk.CombineMeshes(Color.white);
        }
        else
        {
            currentChunk.CombineMeshes(chunkType.pixelColor);
        }

        if(chunkType.IsWalkable())
        {
            walkableSurfaces.Add(currentChunk);
        }
        if(chunkType.name == "Door")
        {
            Building door = GamePlayMode.Instance.BuildOnChunk(DoorPrefab, currentChunk);
            door.transform.SetParent(Ship.Instance.transform);
        }
    }


    private void SortChunksIntoCollections()
    {
        foreach(Chunk chunk in allChunks)
        {
            ChunkType chunkType = chunk.GetChunkType();
            if (!chunkType.IsBasic() && !chunkType.IsInteractable() && chunk.MyCollection == null)  //Dont add interactables yet until we know its side blocks
            {

                Collection collection = new Collection(chunkType);

                if (collection == null) continue;


                collection.AddChunk(chunk);

                Pair chunkCoords = chunk.MyCoords;
                int i = chunkCoords.Column;
                int j = chunkCoords.Row;
                List<Pair> fourGrid = new List<Pair>
                    {
                        new Pair(i + 1, j),
                        new Pair(i - 1, j),
                        new Pair(i, j + 1),
                        new Pair(i, j - 1)
                    };
                foreach (Pair curPair in fourGrid)
                {
                    AddChunkToCollection(curPair, collection, chunkType);
                }

                if (foundInteractable)
                {
                    collection.AddSpecialBuilding(foundInteractable);
                    foundInteractable = null;
                }
                Ship.Instance.AddCollectionToShip(collection);

            }
        }
    }

    private void AddChunkToCollection(Pair chunkCoord, Collection collection, ChunkType chunkType)
    {
        int Column = chunkCoord.Column;
        int Row = chunkCoord.Row;
        if (Column < 0 || Row < 0) return;  // Make sure you arent looking into the negatives
        Chunk chunk = gridChunks[Column, Row];
        if(chunk && chunk.MyCollection == null)
        {
            ChunkType currentChunkType = chunk.GetChunkType();
            //Make sure its either the same chunk type or an interactable
            if(currentChunkType.IsInteractable() || currentChunkType == chunkType)
            {
                if (currentChunkType.IsInteractable())
                {
                    ChunkTypeCollection CTShipPiece = (ChunkTypeCollection)chunkType;
                    if(CTShipPiece)
                    {
                        foundInteractable = chunk;
                        chunk.SetChunkType(allChunkTypes.GetFloor());
                    }
                    else
                    {
                        return;
                    }

                }
                collection.AddChunk(chunk);


                List<Pair> fourGrid = new List<Pair>
                    {
                        new Pair(Column + 1, Row),
                        new Pair(Column - 1, Row),
                        new Pair(Column, Row + 1),
                        new Pair(Column, Row - 1)
                    };
                foreach (Pair curPair in fourGrid)
                {
                    AddChunkToCollection(curPair, collection, chunkType);
                }
                fourGrid.Clear();
            }
        }

    }

    public float GetHeight(HeightType heightType)
    {
        switch (heightType)
        {
            case HeightType.Normal:
                return heightNormal;
            case HeightType.Medium:
                return heightMedium;
            case HeightType.High:
                return heightHigh;
            case HeightType.Max:
                return heightMax;
            case HeightType.Empty:
                return heightEmpty;
            default:
                return heightNormal;
        }
     
    }

    public float GetMultiplier(MultiplierType multiplierType)
    {
        switch (multiplierType)
        {
            case MultiplierType.VerySlow:
                return verySlowMultiplier;
            case MultiplierType.Slow:
                return slowMultiplier;
            case MultiplierType.Medium:
                return mediumMultiplier;
            case MultiplierType.Fast:
                return fastMultiplier;
            case MultiplierType.Full:
                return 1f;
            case MultiplierType.None:
                return 0f;

        }
        return 0f;
    }

    public Color GetCorruptColor(float corruption)
    {
        //Because this has a sprite, you want it to be pure in the end.
        return Color.Lerp(Color.white, corruptionColorBase, corruption);
    }

    public Color GetCorruptColor(float corruption, Color baseColor)
    {
        return Color.Lerp(baseColor, Color.Lerp(baseColor, corruptionColorBase, corruptionColorAlpha), corruption);
    }
}



public enum HeightType { Normal, Medium, High, Max, Empty }
public enum MultiplierType { None, VerySlow, Slow, Medium, Fast, Full}
[System.Serializable]
public class Pair
{
    public int Column { get; private set; }
    public int Row { get; private set; }
    public Pair(int column, int row)
    {
        this.Column = column;
        this.Row = row;
    }

    public override string ToString()
    {
        return "Column: " + Column + " Row: " + Row;
    }
}


