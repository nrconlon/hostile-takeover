﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GridSquare : MonoBehaviour {

    private Color corruptedColor;




    private float secondsToCorrupt; //  DECIDES HOW QUICKLY.  
    private float spreadCorruptionThreshhold = 0; 
    private float spreadCorruptionThreshholdBase; // DECIDES HOW SPREADY :  Is based off seconds so that the spread ratio stays the same
    private float HowOftenCorruptionTick;  //Set from Map Controller


    private int chunkWidth;           //Set from Map Controller

    private Chunk myChunk;




    private ChunkType chunkType;
    private Pair myCoords;

    //public NavMeshSurface surface;
    private float corruption = 0;
    private float selfCorruptionPerTick;
    private float corruptionMultiplier;
    private SpriteRenderer spriteRendererRef;
    private Sprite mySprite;

    float corruptionColorAlpha;

    bool isCorruptionTurnedOn = true;
    bool cured = false;

    [HideInInspector] public bool sideTurnedOff = false;




    // Use this for initialization
    void Awake () {
        spriteRendererRef = GetComponentInChildren<SpriteRenderer>();
    }

    public float GetCorruption()
    {
        return corruption;
    }


    public void SetChunk(Chunk chunk)
    {
        myChunk = chunk;
    }

    public Chunk GetChunk()
    {
        return myChunk;
    }

    public void SetCoords(Pair coords)
    {
        myCoords = coords;
    }

    public Pair GetCoords()
    {
        return myCoords;
    }



    public void SetCorruptionInfo(float secondsToCorrupt, float HowOftenCorruptionTick, float spreadCorruptionThreshold, int chunkWidth)
    {
        spreadCorruptionThreshholdBase = spreadCorruptionThreshold;
        this.secondsToCorrupt = secondsToCorrupt;
        this.HowOftenCorruptionTick = HowOftenCorruptionTick;
        this.chunkWidth = chunkWidth;
    }

    public void SpriteOff()
    {
        spriteRendererRef.enabled = false;
    }

    public void SpriteOn()
    {

        spriteRendererRef.enabled = true;
    }




    public bool IsCorruptable()
    {
        if (!chunkType || chunkType.corruptRate == 0 || !isCorruptionTurnedOn || cured || sideTurnedOff)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool CanSpread()
    {
        return (GetCorruption() >= spreadCorruptionThreshhold);
    }


    public bool IsCorrupted()
    {
        return (GetCorruption() > 0);
    }

    public bool ApplyCorruption()
    {
        return ApplyCorruption(selfCorruptionPerTick);
    }


    /// <summary>
    /// Fails if grid is space, if grid is 1, or if add corruption was 0.  
    /// </summary>
    /// <param name="addCorruption"></param>
    /// <returns></returns>
    private bool ApplyCorruption(float addCorruption)
    {
        addCorruption *= corruptionMultiplier;
        float newCorruption = Mathf.Clamp(GetCorruption() + addCorruption, 0, 1);
        if (newCorruption == GetCorruption())
        {
            return false;
        }
        else
        {
            if (myChunk)
            {
                float corruptionChange = newCorruption - corruption;
                myChunk.ApplyCorruption(corruptionChange);
            }
            corruption = newCorruption;

            if (chunkType.sprite)
            {
                spriteRendererRef.color = Color.Lerp(Color.white, corruptedColor, corruptionColorAlpha * GetCorruption());
            }
            else
            {
                spriteRendererRef.color = Color.Lerp(chunkType.pixelColor, Color.Lerp(chunkType.pixelColor, corruptedColor, corruptionColorAlpha), GetCorruption());
            }

            return true;
        }

    }

    internal void RemoveCorruptionTick(float corruptionRemoveMultiplier)
    {
        ApplyCorruption(corruptionRemoveMultiplier * selfCorruptionPerTick);
    }

    public void SetChunkType(ChunkType type)
    {
        if (type == null)
        {
            transform.position = new Vector3(transform.position.x, -5f, transform.position.z);
        }
        else
        {
            chunkType = type;
            gameObject.name = chunkType.name;
            secondsToCorrupt = secondsToCorrupt / HowOftenCorruptionTick;
            selfCorruptionPerTick = 1f / secondsToCorrupt;
            corruptionMultiplier = MapController.Instance.GetMultiplier(type.corruptRate);
            spreadCorruptionThreshhold = Mathf.Clamp(((spreadCorruptionThreshholdBase * ((secondsToCorrupt / corruptionMultiplier) / (60 / HowOftenCorruptionTick))) * chunkWidth), 0, 1);

            if(chunkType.sprite)
            {
                float pixelWidth = 0;
                float pixelHeight = 0;
                if (chunkType.sprite.texture.width % chunkWidth != 0 || chunkType.sprite.texture.height % chunkWidth != 0)
                {
                    Debug.Log("Sprite cant be devided to chunk.  Width/Height : " + chunkType.sprite.texture.width + "/" + chunkType.sprite.texture.height);
                }
                else
                {
                    pixelWidth = chunkType.sprite.texture.width / chunkWidth;
                    pixelHeight = chunkType.sprite.texture.height / chunkWidth;
                    Rect tempRect = new Rect((myCoords.Column % chunkWidth) * pixelWidth, (myCoords.Row % chunkWidth) * pixelHeight, pixelWidth, pixelHeight);
                    Sprite newSprite = Sprite.Create(chunkType.sprite.texture, tempRect, new Vector2(0.5f, 0.5f));
                    spriteRendererRef.sprite = newSprite;
                    spriteRendererRef.transform.localScale = new Vector3(chunkWidth, chunkWidth, chunkWidth);
                    transform.position = new Vector3(transform.position.x, 0, transform.position.z);
                }
            }
            else
            {
                spriteRendererRef.color = type.pixelColor;
                spriteRendererRef.transform.localScale = new Vector3(1, 1, 1);
                transform.position = new Vector3(transform.position.x, -MapController.Instance.GetHeight(chunkType.height), transform.position.z);
            }

        }


    }



    public void SetCorruptedColor(Color corruptedColor, float corruptionColorAlpha)
    {
        this.corruptedColor = corruptedColor;
        this.corruptionColorAlpha = corruptionColorAlpha;
    }

    internal void SetCorruptionOn(bool corruptionTurnedOn)
    {
        this.isCorruptionTurnedOn = corruptionTurnedOn;
    }

    public void SetCured(bool newCured)
    {
        cured = newCured;
    }
}
