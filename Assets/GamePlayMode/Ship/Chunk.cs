﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class Chunk : MonoBehaviour {
    private ChunkType chunkType;


    private Collection myCollection;

    private Building myBuilding = null;

    private Color defaultColor;
    private Color corruptedColor;

    private float corruptionColorAlpha;

    private float corruption = 0;
    private float corruptionMax = 0;
    private float corruptionTotal = 0;
    private NavMeshSurface[] navSurfaces; // one surface per agent


    public SpriteRenderer spriteRenderer;

    public List<GridSquare> myGridSquares = new List<GridSquare>();

    bool isCombinedMesh = false;
    public int floorTier = 0; //0 for floor, 1 for corrupted, 2 for layered, 3 for sealed.  higher the number, the more powerful the corruption.

    bool isCorruptionTurnedOn = true;
    bool isConnectedToSeed = false;
    bool cured = false;
    bool isRoom = false;



    private void Awake()
    {
        //meshRenderer = GetComponent<SpriteRenderer>();// Set in prefab
        navSurfaces = GetComponentsInChildren<NavMeshSurface>();

    }



    public void SetChunkType(ChunkType chunkType)
    {
        if (chunkType == null)
        {
            transform.position = new Vector3(transform.position.x, 5f, transform.position.z);
        }
        else
        {
            this.chunkType = chunkType;
            gameObject.name = this.chunkType.name;
            if (chunkType.sprite)
            {
                spriteRenderer.sprite = chunkType.sprite;
            }
            transform.position = new Vector3(transform.position.x, MapController.Instance.GetHeight(chunkType.height), transform.position.z);
        }
        foreach (GridSquare gridSquare in myGridSquares)
        {
            gridSquare.SetChunkType(chunkType);
        }
    }

    public void ChangeChunkType(ChunkType chunkType)
    {
        SetChunkType(chunkType);
        BuildNavMesh();
    }


    public ChunkType GetChunkType()
    {
        return chunkType;
    }


    public void SetColors(Color corruptedColor, float corruptionColorAlpha)
    {
        this.corruptedColor = corruptedColor;
        this.corruptionColorAlpha = corruptionColorAlpha;
    }



    private void RecalculateCorruption()
    {
        float newCorruption = corruptionTotal/corruptionMax;
        if (newCorruption != GetCorruption())
        {
            if (newCorruption >= 0.99f && !IsCombinedMesh())
            {
                floorTier = 1;
                newCorruption = 1;
                if(chunkType.sprite)
                {
                    CombineMeshes(Color.Lerp(Color.white, corruptedColor, corruptionColorAlpha));
                }
                else
                {
                    CombineMeshes(Color.Lerp(chunkType.pixelColor, corruptedColor, corruptionColorAlpha));
                }

            }
            else if (newCorruption <= 0f && !IsCombinedMesh())
            {
                floorTier = 0;
                newCorruption = 0;

                if (chunkType.sprite)
                {
                    CombineMeshes(Color.white);
                }
                else
                {
                    CombineMeshes(chunkType.pixelColor);
                }

            }
            else if (IsCombinedMesh())
            {
                floorTier = 0;
                SplitMeshes();
            }


            SetCorruption(newCorruption);
        }
    }

    private void SetCorruption(float newCorruption)
    {
        if (myCollection != null)
        {
            float corruptionChange = newCorruption - corruption;
            myCollection.ApplyCorruption(corruptionChange);
        }
        corruption = newCorruption;
    }

    public float GetCorruption()
    {
        return corruption;
    }

    public bool IsCorrupted()
    {
        return (GetCorruption() > 0);
    }

    public bool IsMoreCorruptedThanOrEqual(float threshold)
    {
        return (GetCorruption() >= threshold);
    }

    public bool IsCorruptableToStart()
    {
        if (chunkType.corruptRate == 0 || !isCorruptionTurnedOn || cured || !isConnectedToSeed)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool IsConnectable()
    {
        if (chunkType.corruptRate == 0 || !isCorruptionTurnedOn || cured)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void ApplyCorruption(float addCorruption)
    {
        corruptionTotal = Mathf.Clamp(corruptionTotal + addCorruption, 0, corruptionMax);
        RecalculateCorruption();

    }

    public void AddGridSquare(GridSquare gridSquare)
    {
        myGridSquares.Add(gridSquare);
        corruptionMax += 1;
        corruptionTotal += gridSquare.GetCorruption();
        RecalculateCorruption();
    }

    public void RemoveGridSquare(GridSquare gridSquare)
    {
        myGridSquares.Remove(gridSquare);
        corruptionMax -= 1;
        corruptionTotal -= gridSquare.GetCorruption();
        RecalculateCorruption();
    }

    public Pair MyCoords { get; set; }

    public Collection MyCollection
    {
        get
        {
            return myCollection;
        }

        set
        {
            myCollection = value;
        }
    }

    public Building MyBuilding
    {
        get
        {
            return myBuilding;
        }

        set
        {
            if (value.buildingType == BuildingType.TheSeed)
            {
                SetConnectedToSeed(true);
            }
            myBuilding = value;
            value.DiedEvent.AddListener(LostMyBuilding);
            if(value.IsCorruptable())
            {
                MapController.Instance.chunksWithBuildings.Add(this);
            }

        }
    }

    void LostMyBuilding(SelectableObject selectable)
    {
        myBuilding = null;
        MapController.Instance.chunksWithBuildings.Remove(this);
    }

    public void BuildNavMesh()
    {
        foreach (NavMeshSurface surface in navSurfaces)
        {
            surface.BuildNavMesh();
        }

    }

    public void TurnOnNavMesh()
    {
        foreach (NavMeshSurface surface in navSurfaces)
        {
            surface.enabled = true;
        }
    }

    public float GetCorruptionMax()
    {
        return corruptionMax;
    }

    public float GetCorruptionTotal()
    {
        return corruptionTotal;
    }

    public void CombineMeshes(Color color)
    {
        if (!isCombinedMesh)
        {
            isCombinedMesh = true;
            foreach (GridSquare child in myGridSquares)
            {
                child.SpriteOff();
            }
        }

        AdjustColor(color);
        spriteRenderer.enabled = true;


    }

    public void AdjustColor(Color color)
    {
        if (chunkType.IsAlien())
        {
            spriteRenderer.color = Color.white;
        }
        else
        {
            spriteRenderer.color = color;
        }
    }

    private void SplitMeshes()
    {
        isCombinedMesh = false;
        foreach (GridSquare child in myGridSquares)
        {
            child.SpriteOn();
        }
        spriteRenderer.enabled = false;
    }


    public bool IsCombinedMesh()
    {
        return isCombinedMesh;
    }

    public bool SetCorruption(bool corruptionSet)
    {
        //Cant turn seed off
        if(myBuilding && myBuilding.buildingType == BuildingType.TheSeed)
        {
            isCorruptionTurnedOn = true;
        }
        else
        {

            isCorruptionTurnedOn = corruptionSet;
        }
        foreach (GridSquare child in myGridSquares)
        {
            child.SetCorruptionOn(isCorruptionTurnedOn);
        }
        return isCorruptionTurnedOn;


    }

    public bool IsCorruptionTurnedOn()
    {
        return isCorruptionTurnedOn;
    }

    public void SetConnectedToSeed(bool connected)
    {
        //Connected means there is corruption and a good path leading to it. Its only uses for the initial adding to the list
        isConnectedToSeed = connected;
    }

    public void SetCured(bool newCured)
    {
        cured = newCured;
        foreach (GridSquare child in myGridSquares)
        {
            child.SetCured(newCured);
        }
    }

    public void TurnOnSides()
    {
        foreach (GridSquare square in myGridSquares)
        {
            square.sideTurnedOff = false;
        }
    }

    internal void CorruptBuilding()
    {
        if(myBuilding)
        {
            myBuilding.ApplyCorruption(corruption);
        }
    }
}

public enum Direction { WEST, EAST, NORTH, SOUTH }