﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EditorObjects/Map/ListOfMaps")]
public class ListOfMaps : ScriptableObject
{
    public List<MapInfo> myMaps;
    public Texture2D GetMap(string s)
    {
        foreach (MapInfo mapLookup in myMaps)
        {

            if (mapLookup.name.Equals(s))
            {
                return mapLookup.map;
            }
        }
        return null;
    }

}

