﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "EditorObjects/Map/MapInfo")]
public class MapInfo : ScriptableObject
{
    public int minHumans;
    public int maxHumans;
    public ChunkType dontSpawnHumansInCollection;
    public Texture2D map;
}