﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EditorObjects/Map/MapHolder")]
public class MapHolder : ScriptableObject
{

    public MapInfo myMap;
}
