﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "EditorObjects/Item/Buildable")]
public class BuildableItem : InventoryItem {

    public GameObject Building;
    public ChunkPreferences buildingPreferences;
    int NumberOfCharges = 1;

    public override void WasUsed()
    {
        NumberOfCharges--;
        if(NumberOfCharges < 1)
        {
            myHolder.DeleteItem(this);
        }
    }

    public override void Initialize()
    {
        myAbility = new BuildItemAbility(this, Building, buildingPreferences);
    }
}
