﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class ItemAbility {

    protected InventoryItem myItem;



    public abstract void Activate();


}
