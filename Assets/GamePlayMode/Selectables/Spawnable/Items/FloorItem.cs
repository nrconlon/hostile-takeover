﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorItem : SelectableObject {
    public InventoryItem myItem;
    protected override void Awake()
    {
        base.Awake();
        SetTeam(TeamStatus.Neutral);
    }

    public void AssignItem(InventoryItem item)
    {
        icon = item.icon;
        myItem = item;
        spriteRendererRef.sprite = item.sprite;
        description = item.description;

    }


}


