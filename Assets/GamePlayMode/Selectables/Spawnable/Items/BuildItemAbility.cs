﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildItemAbility : ItemAbility {



    private GameObject buildingObject;
    private ChunkPreferences preferences;
    private Chunk targetChunk;

    public BuildItemAbility(InventoryItem myItem, GameObject buildingObject, ChunkPreferences preferences)
    {
        this.myItem = myItem;
        this.buildingObject = buildingObject;
        this.preferences = preferences;
    }


    public override void Activate()
    {
        EventChunk eventChunk = new EventChunk();
        eventChunk.AddListener(Use);
        Building building = buildingObject.GetComponent<Building>();
        PlayerInputHandler.Instance.AbilityChunk(eventChunk, preferences, building.GetSprite());
    }

    public void Use(Chunk chunk)
    {
        targetChunk = chunk;
        Vector3 position = chunk.transform.position;

        if(myItem.myHolder)
        {
            SpriteRenderer buildingSprite = PlayerInputHandler.Instance.GetBuildingSpriteRenderer();
            buildingSprite.sprite = buildingObject.GetComponent<Building>().GetSprite();
            EventFloor build = new EventFloor();
            build.AddListener(Build);
            myItem.myHolder.MoveAndBuildOnChunk(build, position, buildingSprite.gameObject, 0.7f);

        }
        else
        {
            Build(position);
        }


    }

    public void Build(Vector3 position)
    {
        myItem.myHolder.BuildOnChunk(buildingObject, targetChunk);
        myItem.WasUsed();
    }
}

