﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public abstract class InventoryItem : ScriptableObject {

    public ItemSlotType mySlotType;
    public Sprite icon;
    public Sprite sprite;
    [HideInInspector] public Unit myHolder;

    public new string name = "Item Name";
    public string description = "Description of Item, read while in Inventory";

    public ItemAbility myAbility;


    public virtual void PickedUp(Unit holder)
    {
        myHolder = holder;
    }
    public virtual void Dropped()
    {
        myHolder = null;
    }
    public abstract void WasUsed();
    public abstract void Initialize();

}
public enum ItemSlotType { HEAD, BODY, BACK, MAIN, SIDE }