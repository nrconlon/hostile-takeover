﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using SwordGC.AI.Goap;
using UnityEngine.Events;
using System;

public class Unit : SelectableObject
{
	private NavMeshAgent myNavAgent;
    bool canOpenDoor = true; 
    public int upkeepCost;



    Vector3 destination;
    float distanceFromObject = 0.1f;
    SelectableObject targetToTouch;
    EventFloor arrivedEvent = null;
    EventSelectable arrivedEventSelectable = null;



    public bool canHoldHelm = false;
    public bool canHoldBody = false;


    public bool canHoldMain = false;
    public bool canHoldBack = false;
    public bool canHoldSide = false;
    public bool canHoldSideSecond = false;

    float bodyArmor = 0f;
    float headArmor = 0f;


    [HideInInspector] public InventoryItem helmitItem;
    [HideInInspector] public InventoryItem bodyItem;
    [HideInInspector] public InventoryItem mainItem;
    [HideInInspector] public InventoryItem backItem;
    [HideInInspector] public InventoryItem sideItem;
    [HideInInspector] public InventoryItem sideSecondItem;

    private InventoryItem itemBeingGivenUp;
    private GameObject curentBuildingSprite;
    private EventSelectable MainAttack;
    private SelectPreferences mainAttackPreferences;
    [HideInInspector] UnityEvent cancelStationaryChanneling;

    public bool spriteTurnsWithDirection = true;

    private float oldx = 90;
    private float oldy = 0;
    private int level = 1;







    // Use this for initialization
    protected override void Start () {
        myNavAgent = GetComponent<NavMeshAgent>();
		destination = transform.position;
		myNavAgent.updateRotation = false;
		myNavAgent.updatePosition = true;
        myNavAgent.Warp(transform.position);
        oldx = spriteRendererRef.transform.rotation.eulerAngles.x;
        oldy = spriteRendererRef.transform.rotation.eulerAngles.y;
        base.Start();

    }
	
	// Update is called once per frame
	public virtual void Update () {
        if(arrivedEvent != null)
        {
            if(Vector3.Distance(destination,transform.position) < distanceFromObject)
            {

                arrivedEvent.Invoke(destination);
                ResetMoveAndPerform();
            }
        }
        else if(arrivedEventSelectable != null)
        {
            Vector3 targetPosition = targetToTouch.transform.position;
            if (Vector3.Distance(targetPosition, transform.position) < distanceFromObject)
            {
                arrivedEventSelectable.Invoke(targetToTouch);
                ResetMoveAndPerform();
            }
            else
            {
                SetDestination(targetPosition);
            }
        }

        if (spriteTurnsWithDirection && myNavAgent.velocity != Vector3.zero)
        {
            spriteRendererRef.transform.rotation = Quaternion.Euler(oldx, oldy, Vector2.SignedAngle(new Vector2(0,-1), new Vector2(myNavAgent.velocity.x, myNavAgent.velocity.z)));
        }
	}


    public bool CanOpenDoor()
    {
        return canOpenDoor;
    }

	public void SetDestination(Vector3 newDestination)
	{
        if(destination != newDestination)
        {
            if (newDestination == Vector3.zero)
            {
                if (myNavAgent.isOnNavMesh)
                {
                    myNavAgent.SetDestination(transform.position);
                }

            }
            else
            {
                if (myNavAgent.isOnNavMesh)
                {
                    destination = newDestination;
                    myNavAgent.SetDestination(destination);
                }

            }

        }

    }

    bool CantMove()
    {
        return IsDead();
    }

    public override void RightClickedFloor(Vector3 floorPoint)
    {
        if (CantMove()) return;
        ResetAll();
        myNavAgent.stoppingDistance = 0;
        SetDestination(floorPoint);
    }

    public void RightClickedFloor(Vector3 floorPoint, float stoppingDistance)
    {
        if (CantMove()) return;
        ResetAll();
        myNavAgent.stoppingDistance = stoppingDistance;
        SetDestination(floorPoint);
    }

    public override void RightClickedSelectable(SelectableObject selectableObject)
    {
        if (CantMove()) return;
        ResetAll();
        SetTarget(selectableObject);
    }


    private void ClearSpriteRenderer()
    {
        Destroy(curentBuildingSprite);
        curentBuildingSprite = null;
    }

    public void ResetMoveAndPerform()
    {
        if (curentBuildingSprite)
        {
            ClearSpriteRenderer();
        }

        distanceFromObject = 0;
        targetToTouch = null;
        arrivedEvent = null;
        arrivedEventSelectable = null;
        SetDestination(Vector3.zero);
    }

    public void ResetAll()
    {
        if (cancelStationaryChanneling != null)
        {
            StopChanneling();

        }
        ResetMoveAndPerform();
    }

    public void SetTarget(SelectableObject target)
    {
        //TODO check if building and act accordingly
        switch (target.GetTeam())
        {
            case TeamStatus.Alien:
                FollowTarget(target);
                break;
            case TeamStatus.Neutral:
                InteractWithThing(target);
                break;
            case TeamStatus.Human:
                if(mainAttackPreferences.FitsPreferences(target))
                {
                    AttackTarget(target);
                }
                else
                {
                    FollowTarget(target);
                }
                break;
            case TeamStatus.None:
                Debug.Log("this target has no team, what should I do? Target: " + target);
                InteractWithThing(target);
                break;
            default:
                break;
        }
    }

    public virtual void FollowTarget(SelectableObject target)
    {
        //TODO Keep certain distance, follow depending on if its a collection (get nearest point) or spawnable.  It might even mean pick it up if its an item.
        SetDestination(target.transform.position);
    }

    public void MoveAndPerform(SelectableObject target, EventSelectable arrived, float distanceFromTarget = 0f)
    {
        ResetMoveAndPerform();
        distanceFromTarget += (target.transform.localScale.x + transform.localScale.x) / 2;
        //Flash circle on it to say it was right clicked on
        distanceFromObject = distanceFromTarget;
        arrivedEventSelectable = arrived;
        targetToTouch = target;
        SetDestination(target.transform.position);
    }

    public void MoveAndPerform(EventFloor arrived, Vector3 location, float distanceFromTarget = 0.11f)
    {
        ResetMoveAndPerform();
        distanceFromObject = distanceFromTarget;
        arrivedEvent = arrived;
        SetDestination(location);

    }

    public void MoveAndBuildOnChunk(EventFloor arrived, Vector3 location, GameObject buildingSprite, float distanceFromTarget = 0.11f)
    {
        MoveAndPerform(arrived, location, 0.7f);
        curentBuildingSprite = buildingSprite;

    }


    public virtual void AttackTarget(SelectableObject target)
    {
        if(MainAttack != null)
        {
            MainAttack.Invoke(target);
        }
        else
        {
            FollowTarget(target);
        }
    }



    public void SetMainAttack(EventSelectable eventSelectable, SelectPreferences preferences)
    {
        MainAttack = eventSelectable;
        mainAttackPreferences = preferences;
    }

    public override bool ApplyAttack(Attack attack)
    {

        if (attack.headAttack)
        {
            if (attack.piercing < headArmor)
            {
                return false;
            }
        }
        else
        {
            if (attack.piercing < bodyArmor)
            {
                return false;
            }
        }


        if(base.ApplyAttack(attack))
        {
            //Suspicion
            //Corruption
            return true;
        }
        else
        {
            return false;
        }

    }


    public virtual void InteractWithThing(SelectableObject target)
    {
        if (target is FloorItem)
        {
            arrivedEventSelectable = new EventSelectable();
            arrivedEventSelectable.AddListener(PickingUpItem);
            MoveAndPerform(target, arrivedEventSelectable);
        }
        else
        {
            SetDestination(target.transform.position);
        }
    }



    public void DeleteItem(InventoryItem item)
    {
        RemoveItemFromInventory(item);
        Destroy(item);
    }

    public void DropItem(InventoryItem item, Vector3 location)
    {
        itemBeingGivenUp = item;
        EventFloor dropItemEvent = new EventFloor();
        dropItemEvent.AddListener(DroppedItem);
        MoveAndPerform(dropItemEvent, location,0.2f);
    }

    public void GiveItem(InventoryItem item, SelectableObject target)
    {
        itemBeingGivenUp = item;
        EventSelectable giveItemEvent = new EventSelectable();
        giveItemEvent.AddListener(GivenItem);
        MoveAndPerform(target, giveItemEvent, 0.2f);
    }

    private void GivenItem(SelectableObject target)
    {

        if(target.RecieveItem(itemBeingGivenUp))
        {
            RemoveItemFromInventory(itemBeingGivenUp);
        }
        else
        {
            Debug.Log("Target couldn't take Item anymore");
        }
    }

    public void DroppedItem(InventoryItem item)
    {
        DroppedItem(item, Vector3.zero);
    }

    private void DroppedItem(Vector3 location)
    {
        DroppedItem(itemBeingGivenUp, location);
    }

    public void DroppedItem(InventoryItem item, Vector3 location)
    {
        RemoveItemFromInventory(item);
        if(location != Vector3.zero)
        {
            GamePlayMode.Instance.DropItem(location, item);
        }
        else
        {
            GamePlayMode.Instance.DropItem(transform.position, item);
        }
        
    }

    private void RemoveItemFromInventory(InventoryItem item)
    {
        ItemSlotType slotType = item.mySlotType;
        switch (slotType)
        {
            case ItemSlotType.HEAD:
                helmitItem = null;
                break;
            case ItemSlotType.BODY:
                bodyItem = null;
                break;
            case ItemSlotType.BACK:
                backItem = null;
                break;
            case ItemSlotType.MAIN:
                mainItem = null;
                break;
            case ItemSlotType.SIDE:
                if (sideItem == item)
                {
                    sideItem = null;
                }
                else
                {
                    sideSecondItem = null;
                }
                break;
            default:
                break;
        }
        GamePlayMode.Instance.UpdateAbilitiesAndInfoPanels();
    }

    public void SetItemInInventory(InventoryItem item)
    {
        ItemSlotType slotType = item.mySlotType;
        switch (slotType)
        {
            case ItemSlotType.HEAD:
                if (helmitItem)
                {
                    DroppedItem(helmitItem);
                }
                helmitItem = item;
                break;
            case ItemSlotType.BODY:
                if (bodyItem)
                {
                    DroppedItem(bodyItem);
                }
                bodyItem = item;
                break;
            case ItemSlotType.MAIN:
                if (mainItem)
                {
                    DroppedItem(mainItem);
                }
                mainItem = item;
                break;
            case ItemSlotType.BACK:
                if (backItem)
                {
                    DroppedItem(backItem);
                }
                backItem = item;
                break;
            case ItemSlotType.SIDE:
                if(sideItem)
                {
                    if(canHoldSideSecond)
                    {
                        if (sideSecondItem)
                        {
                            DroppedItem(sideSecondItem);
                            sideSecondItem = sideItem;
                            sideItem = item;
                        }
                        else
                        {
                            sideSecondItem = item;
                        }
                    }
                    else
                    {
                        DroppedItem(sideItem);
                        sideItem = item;
                    }
                   
                }
                else
                {
                    sideItem = item;
                }
                break;
            default:
                break;
        }
        item.PickedUp(this);
        GamePlayMode.Instance.UpdateAbilitiesAndInfoPanels();
    }

    public void PickingUpItem(SelectableObject itemSelectable)
    {
        FloorItem floorItem = (FloorItem)itemSelectable;
        InventoryItem item = floorItem.myItem;
        if (CanPickUpItem(item, true))
        {
            SetItemInInventory(item);
            itemSelectable.DiedEvent.Invoke(itemSelectable);
            Destroy(itemSelectable.gameObject);
        }

    }

    public override bool CanPickUpItem(InventoryItem item, bool forced)
    {
        //If we are not forced to pick it up, then we need to check if we have room, otherwise reject.

        ItemSlotType slotType = item.mySlotType;
        switch (slotType)
        {
            case ItemSlotType.HEAD:
                if (!canHoldHelm)
                {
                    Debug.Log("Unit Can't hold head items");
                    return false;

                }
                else if (!forced && helmitItem)
                {
                    Debug.Log("Unit's inventory is full of that type");
                    return false;
                }
                break;
            case ItemSlotType.BODY:
                if (!canHoldBody)
                {
                    Debug.Log("Unit Can't hold body items");
                    return false;

                }
                else if (!forced && bodyItem)
                {
                    Debug.Log("Unit's inventory is full of that type");
                    return false;
                }
                break;
            case ItemSlotType.MAIN:
                if (!canHoldMain)
                {
                    Debug.Log("Unit Can't hold main items");
                    return false;

                }
                else if (!forced && mainItem)
                {
                    Debug.Log("Unit's inventory is full of that type");
                    return false;
                }
                break;
            case ItemSlotType.BACK:
                if (!canHoldBack)
                {
                    Debug.Log("Unit Can't hold back items");
                    return false;

                }
                else if (!forced && backItem)
                {
                    Debug.Log("Unit's inventory is full of that type");
                    return false;
                }
                break;
            case ItemSlotType.SIDE:
                if (!canHoldSide)
                {
                    Debug.Log("Unit Can't hold side items");
                    return false;
                }
                else if (!forced && sideItem && (!canHoldSideSecond || sideSecondItem))
                {
                    Debug.Log("Unit's inventory is full of that type");
                    return false;
                }
                break;
            default:
                break;
        }
        return true;
    }

    public override bool RecieveItem(InventoryItem item)
    {
        if (CanPickUpItem(item,false))
        {
            SetItemInInventory(item);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void StartedChanneling(UnityEvent cancelChannelingEvent)
    {
        cancelStationaryChanneling = cancelChannelingEvent;
    }

    public void StopChanneling()
    {
        if (cancelStationaryChanneling != null)
        {
            cancelStationaryChanneling.Invoke();
            cancelStationaryChanneling = null;
        }

    }

    public int GetLevel()
    {
        return level;
    }

    public int CompareUnits(Unit other)
    {
        //return less than zero means the instance (this) precedes the object (other)
        //lower priority = more important. Starts at 1 for most important.
        if (level > other.GetLevel())
        {
            return -1;
        }
        else if (level == other.GetLevel())
        {
            if (currentHealth > other.currentHealth)
            {
                return -1;
            }
            else if (currentHealth == other.currentHealth)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }
    }
}

public enum SpecialSelectableType { None, TheSource, Worker, Human}

