﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Building {

    Animator anim;
    bool isLocked = false;
    bool visualIsOpen = false;
    List<Unit> sensedUnits = new List<Unit>();
    UnityEngine.AI.NavMeshObstacle navMeshObstacle;

    protected override void Awake()
    {
        base.Awake();
        anim = GetComponentInChildren<Animator>();
        navMeshObstacle = GetComponent<UnityEngine.AI.NavMeshObstacle>();
        
    }

    public override void Kill()
    {
        base.Kill();
        GetChunk().SetChunkType(MapController.Instance.allChunkTypes.GetFloor());
    }


    protected override void Start()
    {
        base.Start();
        AdjustDoor();
        anim.SetBool("IsOpen", false);
    }
    public void StartedSensingSomething(Unit sensedUnit)
    {
        sensedUnits.Add(sensedUnit);
        AdjustDoor();
    }
    public void StoppedSensingSomething(Unit sensedUnit)
    {
        sensedUnits.Remove(sensedUnit);
        AdjustDoor();
    }
    private void AdjustDoor()
    {
        if (!isLocked)
        {
            bool foundAGoodUnit = false;
            bool foundAWrongUnit = false;
            foreach (Unit unit in sensedUnits)
            {
                if (unit.CanOpenDoor() && HasAccess(unit.myTeamStatus))
                {
                    foundAGoodUnit = true;
                    break;
                }
                else
                {
                    foundAWrongUnit = true;
                }
            }
            if (foundAGoodUnit)
            {
                OpenDoor(myTeamStatus);
            }
            else
            {
                //if found a wrong unit we actually close, otherwise remain open for A* navigation
                CloseDoor(myTeamStatus, foundAWrongUnit);
            }
        }
        else
        {
            if (sensedUnits.Count > 0)
            {
                CloseDoor(myTeamStatus, true);
            }
            else
            {
                CloseDoor(myTeamStatus, false);
            }
        }
    }
    public void OpenDoor(TeamStatus team)
    {
        //Check if access to door
        if (team == myTeamStatus)
        {
            if (!visualIsOpen)
            {
                visualIsOpen = true;
                anim.SetBool("IsOpen", true);
            }
        }
    }
    public void CloseDoor(TeamStatus team, bool actuallyClose)
    {
        if (team == myTeamStatus)
        {
            if (actuallyClose)
            {
                navMeshObstacle.enabled = true;
            }
            else
            {
                navMeshObstacle.enabled = false;
            }
            if (visualIsOpen)
            {
                visualIsOpen = false;
                anim.SetBool("IsOpen", false);
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Unit unit = other.gameObject.GetComponent<Unit>();
        if (unit)
        {
            StartedSensingSomething(unit);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Unit unit = other.gameObject.GetComponent<Unit>();
        if (unit)
        {
            StoppedSensingSomething(unit);
        }
    }

    //void OnCollisionEnter(Collision collision)
    //{
    //    Unit unit = collision.gameObject.GetComponent<Unit>();
    //    if(unit)
    //    {
    //        StartedSensingSomething(unit);
    //    }
    //}

    //private void OnCollisionExit(Collision collision)
    //{
    //    Unit unit = collision.gameObject.GetComponent<Unit>();
    //    if (unit)
    //    {
    //        StoppedSensingSomething(unit);
    //    }
    //}

}
