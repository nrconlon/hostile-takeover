﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : SelectableObject
{
    public float requiredDistance;
    [SerializeField] private float startingSizeMultiplier = 0.2f;
    private Chunk myChunk;
    public float buildingTime;
    private float sizeIncrease;
    private float desiredSize;
    private float tickIncrement = 0.1f;
    public BuildingType buildingType = BuildingType.None;

    [HideInInspector] public Collection myCollection;


    public virtual void Activate(Chunk myChunk)
    {
        this.myChunk = myChunk;
        if (buildingTime != 0)
        {
            desiredSize = transform.localScale.x;
            sizeIncrease = (desiredSize - (startingSizeMultiplier * desiredSize)) / (buildingTime / 0.1f);
            transform.localScale = new Vector3((startingSizeMultiplier * desiredSize), transform.localScale.y, (startingSizeMultiplier * desiredSize));
            InvokeRepeating("GrowBuilding", tickIncrement, tickIncrement);
        }
    }

    public Chunk GetChunk()
    {
        return myChunk;
    }

    private void GrowBuilding()
    {
        Vector3 size = transform.localScale;
        if (transform.localScale.x >= desiredSize)
        {
            CancelInvoke("GrowBuilding");
        }
        else
        {
            transform.localScale = new Vector3(size.x + sizeIncrease, size.y, size.z + sizeIncrease);
        }


    }





}

public enum BuildingType { None, TheSeed }
