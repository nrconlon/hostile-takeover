﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;


public class SelectableObject : MonoBehaviour, IComparable<SelectableObject>, IComparable
{

    public TeamStatus myTeamStatus;
    TeamStatus origionalTeamStatus;
    public Sprite icon;
    public string description = "Description of what this thing does";
    protected List<Ability> myAbilities = new List<Ability>();
    private float corruptionPerTick = 0.1f;



    public static List<SelectableObject> allSelectableObjects = new List<SelectableObject>();
    public static List<SelectableObject> currentlySelected = new List<SelectableObject>();


    public float maxHealth = 100;
    [HideInInspector] public float currentHealth = 100;

    public float maxBioFuel = 0;
    [HideInInspector] public float currentBioFuel = 0;

    public static Sprite selectionCircleFriendly;
    public static Sprite selectionCircleNeutral;
    public static Sprite selectionCircleEnemy;

    public bool Industructable = false;


    SpriteRenderer selectionCircle;

    protected float corruption = 0;

    [HideInInspector] public SpriteRenderer spriteRendererRef;
    Animation animationRef;
    [SerializeField] Sprite deadSprite;
    Sprite mainSprite;


    [SerializeField] private float priority = 49;


    [HideInInspector] public EventSelectable DiedEvent;
    bool isDead = false;
    Fester currentFester;


    public SpecialSelectableType selectableType = SpecialSelectableType.None;





    protected virtual void Awake()
    {
        spriteRendererRef = GetComponentInChildren<SpriteRenderer>();
        mainSprite = spriteRendererRef.sprite;
        Initialize();


        animationRef = GetComponentInChildren<Animation>();
        //PlayGameMode.Instance.AddTo
        currentHealth = maxHealth;
        DiedEvent = new EventSelectable();
        origionalTeamStatus = myTeamStatus;
    }

    public void Initialize()
    {
        //TODO clean this up with revive handling other stuff, and Refresh handling all of this and abilities
        allSelectableObjects.Add(this);
        isDead = false;
        spriteRendererRef.sprite = mainSprite;
        RefreshAbilities();

    }

    public void RefreshAbilities()
    {
        myAbilities.Clear();
        foreach (Ability ability in GetComponents<Ability>())
        {
            myAbilities.Add(ability);
        }
        CanvasController.Instance.UpdateAbilitiesAndInfoPanels();
    }

    private void OnDestroy()
    {
        allSelectableObjects.Remove(this);
        currentlySelected.Remove(this);
    }

    // Use this for initialization
    protected virtual void Start()
    {
        switch (myTeamStatus)
        {
            case TeamStatus.Alien:
                AlienTeam.Instance.AddSelectable(this);
                break;
            case TeamStatus.Neutral:
                break;
            case TeamStatus.Human:
                Ship.Instance.AddSelectable(this);
                break;
            default:
                break;
        }

    }

    public void AddToSelectable()
    {
        allSelectableObjects.Add(this);
    }



    public void ApplyCorruption(float baseCorruption)
    {
        corruption = Mathf.Clamp(corruption + (baseCorruption * corruptionPerTick), 0, 1);
        spriteRendererRef.color = MapController.Instance.GetCorruptColor(corruption);
    }

    protected virtual float GetCorruption()
    {
        return corruption;
    }

    public void KillAndRemove()
    {
        Kill();
        Remove();
    }

    public void KillAndDelete()
    {
        Kill();
        Remove();
        Destroy(gameObject);
    }

    public void ReturnToLife()
    {
        Initialize();
    }

   public virtual void Kill()
    {
        isDead = true;
        CanvasController.Instance.UpdateAbilitiesAndInfoPanels();
        DiedEvent.Invoke(this);
        if(deadSprite)
        {

            spriteRendererRef.sprite = deadSprite;
        }
    }

    public void Remove()
    {
        allSelectableObjects.Remove(this);
        currentlySelected.Remove(this);
        CanvasController.Instance.UpdateAbilitiesAndInfoPanels();
        gameObject.SetActive(false);
    }



    public void OnSelect()
    {
        if (!currentlySelected.Contains(this))
        {
            currentlySelected.Add(this);
            ApplyCircle();
        }

    }

    public void OnSelectSingle()
    {

        if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
        {
            DeselectAll();
        }
        OnSelect();
    }


    public static void DeselectAll()
    {
        foreach (SelectableObject selectableObject in currentlySelected)
        {
            selectableObject.OnDeselect();
        }
        currentlySelected.Clear();
    }


    public static void SelectGroup(List<SelectableObject> group)
    {
        if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
        {
            DeselectAll();
        }
        foreach (SelectableObject selectableObject in group)
        {
            selectableObject.OnSelect();
        }
    }

    public virtual bool ApplyAttack(Attack attack)
    {


        if (!ApplyDamage(attack.damage)) return false;


        return true;
    }



    public void OnDeselect()
    {
        RemoveCircle();
    }

    public Sprite GetSprite()
    {
        if (spriteRendererRef)
        {
            return spriteRendererRef.sprite;
        }
        else
        {
            return GetComponentInChildren<SpriteRenderer>().sprite;
        }

    }


    private void ApplyCircle()
    {
        if (!selectionCircle)
        {
            GameObject circleObject = new GameObject();
            circleObject.transform.SetParent(transform);
            selectionCircle = circleObject.AddComponent<SpriteRenderer>();
            Transform circleTransform = selectionCircle.transform;
            Vector3 targetPosition = transform.position;
            circleTransform.position = new Vector3(targetPosition.x, targetPosition.y, targetPosition.z);
            circleTransform.localEulerAngles = new Vector3(90f, 0, 0);
            circleTransform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        }
        else
        {
            selectionCircle.gameObject.SetActive(true);
        }

        switch (myTeamStatus)
        {
            case TeamStatus.Alien:
                selectionCircle.sprite = selectionCircleFriendly;
                break;
            case TeamStatus.Neutral:
                selectionCircle.sprite = selectionCircleNeutral;
                break;
            default:
                selectionCircle.sprite = selectionCircleEnemy;
                break;
        }


    }

    private void RemoveCircle()
    {
        if (selectionCircle)
        {
            selectionCircle.gameObject.SetActive(false);
        }

    }

    public List<Ability> GetAllAbilities()
    {
        return myAbilities;
    }


    public virtual void SetTeam(TeamStatus teamStatus)
    {
        myTeamStatus = teamStatus;
    }

    public void ResetTeam()
    {
        myTeamStatus = origionalTeamStatus;
    }

    public virtual TeamStatus GetTeam()
    {
        return myTeamStatus;
    }

    public bool IsCorruptable()
    {
        if (myTeamStatus == TeamStatus.Neutral || myTeamStatus == TeamStatus.Human)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public bool HasAccess(TeamStatus teamStatus)
    {
        if (myTeamStatus == TeamStatus.Neutral || myTeamStatus == teamStatus)
        {
            return true;
        }
        return false;
    }

    public virtual void RightClickedFloor(Vector3 floorPoint)
    {
        return;
    }

    public virtual void RightClickedSelectable(SelectableObject selectableObject)
    {
        return;
    }

    public virtual Building BuildOnChunk(GameObject building, Chunk chunk)
    {
        return GamePlayMode.Instance.BuildOnChunk(building, chunk);
    }

    public float GetImportance()
    {
        if(this is Unit)
        {
            return priority;
        }
        else if(this is Building)
        {
            return priority + 50;
        }
        else
        {
            return priority + 100;
        }
    }

    public bool IsDead()
    {
        return isDead;
    }



    public virtual bool RecieveItem(InventoryItem item)
    {
        return false;
    }

    public virtual bool CanPickUpItem(InventoryItem item, bool forced)
    {
        return false;
    }

    public virtual string GetCurrentTaskString()
    {
        return "";
    }

    public virtual string GetHealthString()
    {
        if (currentHealth == 0)
        {
            return "Dead";
        }
        else
        {
            return currentHealth + "/" + maxHealth;
        }
        
    }

    public virtual string GetBioFuelString()
    {
        if (maxBioFuel == 0) return "None";
        return currentBioFuel + "/" + maxBioFuel;
    }

    public float GetHealthPercent()
    {
        if (currentHealth == 0)
        {
            return 0;
        }
        else
        {
            return currentHealth / maxHealth;
        }

    }

    public float GetBioFuelPercent()
    {
        if (currentBioFuel == 0)
        {
            return 0;
        }
        else
        {
            return currentBioFuel / maxBioFuel;
        }

    }

    public void AddBioFuel(float bioFuelPerTick)
    {
        float oldBioFuel = currentBioFuel;
        currentBioFuel = Mathf.Clamp(currentBioFuel + bioFuelPerTick, 0, maxBioFuel);
        if(oldBioFuel != currentBioFuel)
        {
            if (bioFuelPerTick < 0)
            {
                GamePlayMode.Instance.SpawnFloatingText(bioFuelPerTick.ToString(), this.transform, TextColor.BioFuel, true);
            }
            else if(bioFuelPerTick > 0)
            {
                GamePlayMode.Instance.SpawnFloatingText("+" + bioFuelPerTick, this.transform, TextColor.BioFuel, true);
            }

        }

    }



    protected bool ApplyDamage(float damage)
    {
        //TAKE INTO ACCOUNT ARMOR FOR DAMAGE REDUCTION
        if (!isDead)
        {
            if (Industructable || currentHealth <= 0) return false;
            currentHealth = Mathf.Clamp(currentHealth - damage, 0, maxHealth);
            GamePlayMode.Instance.SpawnFloatingText("-" + damage, transform, TextColor.Damage, false);
            if (currentHealth <= 0)
            {
                Kill();
            }
            else
            {

                animationRef.Play();
            }
            return true;
        }
        return false;
        

    }

    public int CompareTo(SelectableObject other)
    {
        //return less than zero means the instance (this) precedes the object (other)
        //lower priority = more important. Starts at 1 for most important.
        if (priority < other.priority)
        {
            return -1;
        }
        else if (priority == other.priority)
        {
            if (this is Unit && other is Unit)
            {
                return ((Unit)this).CompareUnits((Unit)other);
            }
            return 0;
        }
        else
        {
            return 1;
        }
    }


    public void SetSprite(Sprite sprite)
    {
        spriteRendererRef.sprite = sprite;
    }

    public int CompareTo(object obj)
    {
        return CompareTo(obj as SelectableObject);
    }


    public bool FesterBody(Unit festerer, Fester fester)
    {
        if (currentFester != null)
        {
            return false;
        }
        else
        {
            currentFester = fester;
            currentFester.StartFester(festerer,spriteRendererRef.sprite,this);
            Invoke("ConcludeFester", fester.timeBetweenTick * fester.totalTicks);
            return true;
        }
    }

    public void CancelFester()
    {
        CancelInvoke("ConcludeFester");
        currentFester.CancelFester(this);
        currentFester = null;
        Destroy(GetComponent<CancelAbility>());
    }

    void ConcludeFester()
    {
        if (currentFester != null)
        {
            currentFester.ConsumeBody(this);
            Destroy(currentFester.festerer);
            currentFester = null;
            Remove();
            Destroy(gameObject);

        }
    }

    public bool IsHuman()
    {
        return (selectableType == SpecialSelectableType.Human);
    }


}
public enum TeamStatus { Alien, Neutral, Human, None }