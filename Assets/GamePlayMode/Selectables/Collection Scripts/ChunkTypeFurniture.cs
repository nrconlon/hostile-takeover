﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "EditorObjects/Chunks/Furniture")]
public class ChunkTypeFurniture : ChunkType {

    public float useTimeMin;
    public float useTimeMax;
    public float droppedBioFuel = 0;


}
