﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


[CreateAssetMenu(menuName = "EditorObjects/Chunks/ListOfChunkTypes")]
public class ListOfChunkTypes : ScriptableObject
{

    public List<ChunkType> myChunkTypes = new List<ChunkType>();

    public void LoadAll()
    {
        ChunkType[] chunkTypes = Resources.LoadAll<ChunkType>("ChunkTypes");
        myChunkTypes.Clear();
        myChunkTypes = chunkTypes.ToList();
    }
    public ChunkType GetChunkTypeByColor(Color pixel)
    {
        foreach (ChunkType chunkType in myChunkTypes)
        {
            if (chunkType && chunkType.pixelColor.Equals(pixel))
            {
                return chunkType;
            }
        }
        return null;
    }

    public ChunkType GetChunkTypeByName(string name)
    {
        foreach (ChunkType chunkType in myChunkTypes)
        {
            if (chunkType && chunkType.name.Equals(name))
            {
                return chunkType;
            }
        }
        return null;
    }

    public ChunkType GetFloor()
    {
        foreach (ChunkType chunkType in myChunkTypes)
        {
            if (chunkType && chunkType.IsFloor())
            {
                return chunkType;
            }
        }
        return null;
    }
}


