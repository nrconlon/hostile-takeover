﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "EditorObjects/Chunks/ChunkType")]
public class ChunkType : ScriptableObject {
    public Sprite sprite = null;
    public Color pixelColor = Color.white;
    public MultiplierType corruptRate = MultiplierType.Medium;
    public HeightType height = HeightType.Empty;


    public bool IsBasic()
    {
        if(this is ChunkTypeFurniture || this is ChunkTypeCollection)
        {
            return false;
        }
        return true;
    }

    public bool IsInteractable()
    {
        return name == "Interactable";
    }

    public bool IsFloor()
    {
        return name == "Floor";
    }


    public bool IsSpace()
    {
        return name == "Space";
    }

    public bool IsAlienSpawn()
    {
        return name == "Spawn";
    }

    public bool IsWalkable()
    {
        if (height == HeightType.Empty || height == HeightType.Max)
        {
            return false;
        }
        return true;
    }

    public bool IsAlien()
    {
        if(this is ChunkTypeCollection)
        {
            ChunkTypeCollection CTC = (ChunkTypeCollection)this;
            return CTC.isAlienRoom;
        }
        return false;
    }
}



