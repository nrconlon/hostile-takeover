﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "EditorObjects/Chunks/Collection")]
public class ChunkTypeCollection : ChunkType {

    public GameObject interactbleBuilding;
    public bool isAlienRoom = false;

}
