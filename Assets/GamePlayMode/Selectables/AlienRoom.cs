﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienRoom : Collection {


    public AlienRoom(ChunkType chunkType) : base(chunkType)
    {

    }

    public void RefreshBuildingSlots()
    {
        foreach(Chunk chunk in myChunks)
        {

            if(CheckGroupForSpecial(chunk))
            {
                Chunk specialChunk = MapController.Instance.GetChunk(chunk.MyCoords.Column + 1, chunk.MyCoords.Row + 1);
                AddSpecialBuilding(specialChunk);
            }

        }
    }



    private bool CheckGroupForSpecial(Chunk chunk)
    {
        int column = chunk.MyCoords.Column;
        int row = chunk.MyCoords.Row;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (!IsGoodChunkThere(column + i, row + j)) return false;
            }
        }
        return true;
    }

    private bool IsGoodChunkThere(int column, int row)
    {
        Chunk chunk = MapController.Instance.GetChunk(column, row);
        if (chunk && chunk.MyCollection == this && !specialChunks.Contains(chunk))
        {
            return true;
        }
        return false;

    }



    private void ApplyPassiveAbility()
    {
        //use chunk info to activate a passive ability to all units on chunks.  maybe ChunkTypeRoom.UsePassive(myChunks);
    }


}
