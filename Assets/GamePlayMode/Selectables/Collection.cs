﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collection
{
    protected List<Chunk> specialChunks = new List<Chunk>();
    protected ChunkType myChunkType;
    protected List<Chunk> myChunks = new List<Chunk>();
    float corruption;

    protected float corruptionMax = 0;
    protected float corruptionTotal = 0;


    public Collection(ChunkType chunkType)
    {
        myChunkType = chunkType;
    }

    public void AddSpecialBuilding(Chunk chunk)
    {
        ChunkTypeCollection collectionType = (ChunkTypeCollection)myChunkType;
        if (collectionType && !specialChunks.Contains(chunk))
        {
            specialChunks.Add(chunk);

            if (collectionType.interactbleBuilding)
            {
                Building interactable = GamePlayMode.Instance.BuildOnChunk(collectionType.interactbleBuilding, chunk);
                interactable.myCollection = this;
            }
            
        }
        else if (!collectionType)
        {
            Debug.Log("aw, yah fucked up now aaron");
        }

    }



    protected virtual void SetCorruption(float newCorruption)
    {
        corruption = newCorruption;
    }

    protected virtual float GetCorruption()
    {
        return corruption;
    }

    public ChunkType GetChunkType()
    {
        return myChunkType;
    }


    private void RecalculateCorruption()
    {
        float newCorruption = corruptionTotal / corruptionMax;
        if (newCorruption != GetCorruption())
        {
            SetCorruption(newCorruption);
        }
    }


    public void ApplyCorruption(float addCorruption)
    {
        corruptionTotal = Mathf.Clamp(corruptionTotal + addCorruption, 0, corruptionMax);
        RecalculateCorruption();

    }


    public void AddChunk(Chunk chunk)
    {
        if(!myChunks.Contains(chunk))
        {
            myChunks.Add(chunk);
            chunk.MyCollection = this;
            corruptionMax += chunk.GetCorruptionMax();
            corruptionTotal += chunk.GetCorruptionTotal();
            RecalculateCorruption();
        }

    }

    public void RemoveChunk(Chunk chunk)
    {
        if (!myChunks.Contains(chunk))
        {
            myChunks.Remove(chunk);
            chunk.MyCollection = null;
            corruptionMax -= chunk.GetCorruptionMax();
            corruptionTotal -= chunk.GetCorruptionTotal();
            RecalculateCorruption();
        }

    }

    public Chunk GetInteractChunk()
    {
        if (specialChunks.Count > 0)
        {
            return specialChunks[UnityEngine.Random.Range(0, specialChunks.Count)];
        }
        else
        {
            return myChunks[UnityEngine.Random.Range(0, myChunks.Count)];
        }
    }


}
