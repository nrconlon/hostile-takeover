﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragSelectionHandler : MonoBehaviour,IBeginDragHandler, IDragHandler, IEndDragHandler {


    [SerializeField] Image selectionBoxImage;
    [SerializeField] PlayerInputHandler myHandler;

    Vector2 startPosition;
    Rect selectionRect;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && SelectableObject.currentlySelected.Count > 0)
        {
            SelectableObject.DeselectAll();
        }
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        if (PlayerInputHandler.Instance.IsUsingAbility()) return;
        if (eventData.button == PointerEventData.InputButton.Left && PlayerInputHandler.Instance.GetCurrentInputMode() != InputMode.Persist)
        {
            selectionBoxImage.gameObject.SetActive(true);
            startPosition = eventData.position;
            selectionRect = new Rect();
        }
       
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {

            if (eventData.position.x < startPosition.x)
            {
                selectionRect.xMin = eventData.position.x;
                selectionRect.xMax = startPosition.x;
            }
            else
            {
                selectionRect.xMin = startPosition.x;
                selectionRect.xMax = eventData.position.x;

            }

            if (eventData.position.y < startPosition.y)
            {
                selectionRect.yMin = eventData.position.y;
                selectionRect.yMax = startPosition.y;
            }
            else
            {
                selectionRect.yMin = startPosition.y;
                selectionRect.yMax = eventData.position.y;

            }

            selectionBoxImage.rectTransform.offsetMin = selectionRect.min;
            selectionBoxImage.rectTransform.offsetMax = selectionRect.max;
        }


    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            selectionBoxImage.gameObject.SetActive(false);
            HashSet<SelectableObject> aboutToSelect = new HashSet<SelectableObject>();
            foreach (SelectableObject selectableObject in SelectableObject.allSelectableObjects)
            {
                if (selectionRect.Contains(Camera.main.WorldToScreenPoint(selectableObject.transform.position)))
                {
                    aboutToSelect.Add(selectableObject);
                }
            }

            if (aboutToSelect.Count > 0)
            {
                List<SelectableObject> prioritySelectables = GetPrioritySelectables(aboutToSelect);
                if (prioritySelectables != null)
                {
                    myHandler.SelectGroup(prioritySelectables);
                }
            }
            else
            {
                //Cant grab chunks cuse we dont have access to every chunk.
            }




        }
    }

    private List<SelectableObject> GetPrioritySelectables(HashSet<SelectableObject> startingSet)
    {

        List<SelectableObject> foundAllyUnits = new List<SelectableObject>();
        List<SelectableObject> foundAllyBuildings = new List<SelectableObject>();
        List<SelectableObject> foundEnemyUnits = new List<SelectableObject>();
        List<SelectableObject> foundEnemyBuildings = new List<SelectableObject>();


        foreach (SelectableObject selectableObject in startingSet)
        {
            if (selectableObject is Unit)
            {
                if (selectableObject.GetTeam() == TeamStatus.Alien)
                {

                    foundAllyUnits.Add(selectableObject);
                }
                else
                {
                    foundEnemyUnits.Add(selectableObject);
                }
            }
            else //selectableObject is Building || selectableObject is Collection, or item
            {
                if (selectableObject.GetTeam() == TeamStatus.Alien)
                {

                    foundAllyBuildings.Add(selectableObject);
                }
                else
                {
                    foundEnemyBuildings.Add(selectableObject);
                }
            }
        }

        startingSet.Clear();
        if (foundAllyUnits.Count > 0) return foundAllyUnits;
        else if (foundAllyBuildings.Count > 0) return foundAllyBuildings;
        else if (foundEnemyBuildings.Count > 0) return foundEnemyBuildings;
        else if (foundEnemyUnits.Count > 0) return foundEnemyUnits;
        else
        {
            return null;
        }
    }
}
