﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityButtonControl : MonoBehaviour {
    private Ability myAbility;

    GameObject abilityInfoPanel;
    bool isUsable = true;

    private void Update()
    {
        if (myAbility)
        {
            if (myAbility.IsUsable())
            {
                if (!isUsable)
                {
                    GetComponent<Image>().color = Color.white;
                    isUsable = true;
                }
            }
            else
            {
                if (isUsable)
                {
                    GetComponent<Image>().color = Color.grey;
                    isUsable = false;
                }
            }
        }

    }


    public void SetAbility(Ability ability)
    {
        GetComponent<Image>().sprite = ability.sprite;
        myAbility = ability;

    }


    public void OnAbilitySelect()
    {
        myAbility.Activate();
    }


    public void Hovering()
    {
        CanvasController.Instance.AbilityHovering(myAbility.name, myAbility.description);
    }

    public void StopHovering()
    {
        CanvasController.Instance.AbilityStoppedHovering();
    }

}
