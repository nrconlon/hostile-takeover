﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CorruptionChoiceHandler : MonoBehaviour {
    [SerializeField] Tilemap map;
    public Color corruption;
    public Color leaveAlone;
    Tile tile;

    public void LoadTiles(HashSet<Chunk> positions, Sprite sprite)
    {
        tile = new Tile
        {
            sprite = sprite,
            color = corruption
        };
        foreach (Chunk chunk in positions)
        {
            Pair coords = chunk.MyCoords;
            Vector3Int position = new Vector3Int(coords.Column, coords.Row, 0);
            map.SetTile(position, tile);
        }

    }

    public void SetTile(bool corrupt, Pair coords)
    {
        Vector3Int tileLocation = new Vector3Int(coords.Column, coords.Row, 0);
        if (corrupt)
        {
            tile.color = corruption;
        }
        else
        {
            tile.color = leaveAlone;
        }
        map.SetTile(tileLocation, tile);
        map.RefreshTile(tileLocation);
    }
}
