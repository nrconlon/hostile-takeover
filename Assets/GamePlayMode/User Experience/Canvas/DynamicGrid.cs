﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DynamicGrid : MonoBehaviour {
    // Use this for initialization
    GridLayoutGroup grid;
    RectTransform parent;
    public float numOfColumns = 3;
    public float numOfRows = 2;
    public bool differentDimensions;
    public bool extendWidth = false;
    public bool extendHeight = false;

    void Awake () {
        parent = GetComponent<RectTransform>();
        grid = GetComponent<GridLayoutGroup>();
        Invoke("SetSize", 0.1f);

    }

    public void SetSize()
    {
        float count = transform.childCount;
        float currentNumOfColumns = numOfColumns;
        float currentNumOfRows = numOfRows;
        float comfortableSpace = currentNumOfColumns * currentNumOfRows;
        while(count > comfortableSpace && (extendWidth || extendHeight))
        {
            if (extendWidth && extendHeight)
            {
                currentNumOfColumns++;
                currentNumOfRows++;
            }
            else if (extendWidth)
            {
                currentNumOfColumns++;
            }
            else if (extendHeight)
            {
                currentNumOfRows++;
            }
            comfortableSpace = currentNumOfColumns * currentNumOfRows;
        }


        float size = parent.rect.width / currentNumOfColumns;
        if (differentDimensions)
        {

            float height = parent.rect.height / currentNumOfRows;
            grid.cellSize = new Vector2(size, height);
        }
        else
        {
            grid.cellSize = new Vector2(size, size);
        }


        
    }
	
}
