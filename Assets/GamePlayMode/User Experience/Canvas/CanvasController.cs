﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class CanvasController : MonoBehaviour {

    [SerializeField] Text income;
    [SerializeField] Text upkeep;
    [SerializeField] Text energy;
    [SerializeField] Text bioFuel;




    [SerializeField] RectTransform selectablesAndInfoPanel;
	[SerializeField] DynamicGrid abilitiesPanel;

    [SerializeField] GameObject abilityButtonPrefab;

    [SerializeField] Text abilityTitleRef;
    [SerializeField] Text abilityDescriptionRef;
    [SerializeField] Text abilityHotKeyRef;


    [Header("Item Slots")]
    [SerializeField] ItemButtonHandler itemSlotHelm;
    [SerializeField] ItemButtonHandler itemSlotBody;
    [SerializeField] ItemButtonHandler itemSlotMain;
    [SerializeField] ItemButtonHandler itemSlotBack;
    [SerializeField] ItemButtonHandler itemSlotSide;
    [SerializeField] ItemButtonHandler itemSlotSideSecond;

    [Header("Main Selected Info")]
    [SerializeField] Image mainSelectedIcon;
    [SerializeField] Text mainSelectedCurrentTask;
    [SerializeField] Text mainSelectedHealthText;
    [SerializeField] Image mainSelectedHealthBar;
    [SerializeField] Text mainSelectedBioFuelText;
    [SerializeField] Image mainSelectedBioFuelBar;

    [Header("Selected Info")]
    [SerializeField] DynamicGrid SelectedMultiplePanel;
    [SerializeField] SingleSelectedInfoPanel SelectedSingleInfoPanel;
    [SerializeField] GameObject selectedButtonPrefab;
    List<SelectedButtonControl> allSelectedUnitButtons;

    [Header("Corruption Choice")]
    [SerializeField] RectTransform corruptClearAllPanel;
    [SerializeField] CorruptionChoiceHandler corruptionChoiceHandler;
    [SerializeField] ChunkPreferences corruptionChoicePreferences;
    [SerializeField] Sprite corruptionHandlerSprite;
    [SerializeField] HiveButtonPanel hiveButtonPanel;

    [Header("Hive Ability Panel")]
    [SerializeField] SelectUnitButton workerButton;
    [SerializeField] DynamicGrid unitsPanel;
    [SerializeField] DynamicGrid buildingPanel;
    [SerializeField] DynamicGrid recruitmentPanel;
    [SerializeField] DynamicGrid floorItemsPanel;
    [SerializeField] DynamicGrid shipPanel;
    [SerializeField] Outline corruptHandlerOutline;
    [SerializeField] GameObject selectUnitPrefab;
    private List<DynamicGrid> hiveAbilityPanels;



    List<ItemButtonHandler> allItemSlots;

    SelectableObject mainSelectable;
    

    public static CanvasController Instance { get; private set; }


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
        allSelectedUnitButtons = new List<SelectedButtonControl>();
        allItemSlots = new List<ItemButtonHandler>
        {
            itemSlotHelm,
            itemSlotBody,
            itemSlotMain,
            itemSlotBack,
            itemSlotSide,
            itemSlotSideSecond,
        };

        hiveAbilityPanels = new List<DynamicGrid>
        {
            unitsPanel,
            buildingPanel,
            recruitmentPanel,
            floorItemsPanel,
            shipPanel
        };

        InvokeRepeating("UpdateMainSelectedTick", 0.1f, 0.1f);
    }




    public void UpdateAbilitiesAndInfoPanels()
	{
		if (SelectableObject.currentlySelected.Count > 0)
		{
            SelectableObject.currentlySelected.Sort();
            SelectableObject.currentlySelected.Sort();
            List<Ability> allAbilities = new List<Ability>();
            HashSet<string> abilityNames = new HashSet<string>();
            mainSelectable = SelectableObject.currentlySelected[0];

            foreach (SelectableObject selectableObject in SelectableObject.currentlySelected)
            {

                if (selectableObject.IsDead()) continue;
                foreach (Ability ability in selectableObject.GetAllAbilities())
                {
                    if (!abilityNames.Contains(ability.name))
                    {
                        allAbilities.Add(ability);
                        abilityNames.Add(ability.name);
                    }

                }


            }

            FillAbilitiesPanel(abilitiesPanel,allAbilities);
            SetMain(mainSelectable);
            //FillInfoPanel or fill Selection Panel

        }
        else
        {
            //Clear panel
        }
        UpdateUnits();

	}

    private void UpdateSelectedPanel()
    {
        if (SelectedMultiplePanel.transform.childCount > 0)
        {
            foreach (Transform child in SelectedMultiplePanel.transform)
            {
                Destroy(child.gameObject);
            }
        }
        allSelectedUnitButtons.Clear();

        if (SelectableObject.currentlySelected.Count == 1)
        {
            SelectedSingleInfoPanel.gameObject.SetActive(true);
            SelectedMultiplePanel.gameObject.SetActive(false);
            SelectedSingleInfoPanel.SetMain(mainSelectable);
        }
        else if (SelectableObject.currentlySelected.Count > 1)
        {
            SelectedSingleInfoPanel.gameObject.SetActive(false);
            SelectedMultiplePanel.gameObject.SetActive(true);
            if (allSelectedUnitButtons.Count == 0)
            {
                foreach (SelectableObject selectableObject in SelectableObject.currentlySelected)
                {

                    SelectedButtonControl selectButtonControl = Instantiate(selectedButtonPrefab).GetComponent<SelectedButtonControl>();
                    selectButtonControl.Assign(selectableObject);
                    selectButtonControl.SetToMain(selectableObject == mainSelectable);
                    selectButtonControl.transform.SetParent(SelectedMultiplePanel.transform);
                    allSelectedUnitButtons.Add(selectButtonControl);
                }
            }
            else
            {
                foreach (SelectedButtonControl selectButtonControl in allSelectedUnitButtons)
                {
                    selectButtonControl.SetToMain(mainSelectable);
                }
            }
        }
        SelectedMultiplePanel.SetSize();


    }


    public void SetMain(SelectableObject newMain)
    {

        mainSelectable = newMain;
        UpdateItemsPanel();
        UpdateMainSelected();
        UpdateSelectedPanel();



    }
    private void UpdateMainSelected()
    {
        if (mainSelectable)
        {
            mainSelectedIcon.sprite = mainSelectable.icon;
            UpdateMainSelectedTick();
        }

    }

    private void UpdateMainSelectedTick()
    {

        if (mainSelectable)
        {
            mainSelectedCurrentTask.text = mainSelectable.GetCurrentTaskString();
            mainSelectedHealthText.text = mainSelectable.GetHealthString();
            mainSelectedBioFuelText.text = mainSelectable.GetBioFuelString();
            mainSelectedHealthBar.rectTransform.localScale = new Vector3(mainSelectable.GetHealthPercent(), 1, 1);
            mainSelectedBioFuelBar.rectTransform.localScale = new Vector3(mainSelectable.GetBioFuelPercent(), 1, 1);
        }

    }

    private void UpdateItemsPanel()
    {
        if (mainSelectable is Unit)
        {
            Unit mainUnit = (Unit)mainSelectable;
            if (mainUnit.canHoldHelm)
            {
                itemSlotHelm.Open();
                if(mainUnit.helmitItem)
                {
                    itemSlotHelm.SetItem(mainUnit.helmitItem);
                }
                else
                {
                    itemSlotHelm.ClearItem();
                }
            }
            else
            {
                itemSlotHelm.Close();
            }

            if (mainUnit.canHoldBody)
            {
                itemSlotBody.Open();
                if (mainUnit.bodyItem)
                {
                    itemSlotBody.SetItem(mainUnit.bodyItem);
                }
                else
                {
                    itemSlotBody.ClearItem();
                }
            }
            else
            {
                itemSlotBody.Close();
            }

            if (mainUnit.canHoldMain)
            {
                itemSlotMain.Open();
                if (mainUnit.mainItem)
                {
                    itemSlotMain.SetItem(mainUnit.mainItem);
                }
                else
                {
                    itemSlotMain.ClearItem();
                }
            }
            else
            {
                itemSlotMain.Close();
            }

            if (mainUnit.canHoldBack)
            {
                itemSlotBack.Open();
                if (mainUnit.backItem)
                {
                    itemSlotBack.SetItem(mainUnit.backItem);
                }
                else
                {
                    itemSlotBack.ClearItem();
                }
            }
            else
            {
                itemSlotBack.Close();
            }

            if (mainUnit.canHoldSide)
            {
                itemSlotSide.Open();
                if (mainUnit.sideItem)
                {
                    itemSlotSide.SetItem(mainUnit.sideItem);
                }
                else
                {
                    itemSlotSide.ClearItem();
                }
            }
            else
            {
                itemSlotSide.Close();
            }

            if (mainUnit.canHoldSideSecond)
            {
                itemSlotSideSecond.Open();
                if (mainUnit.sideSecondItem)
                {
                    itemSlotSideSecond.SetItem(mainUnit.sideSecondItem);
                }
                else
                {
                    itemSlotSideSecond.ClearItem();
                }
            }
            else
            {
                itemSlotSideSecond.Close();
            }
        }
        else
        {
            foreach (ItemButtonHandler handler in allItemSlots)
            {
                handler.Close();
            }
        }
    }



    private void FillAbilitiesPanel(DynamicGrid dynamicGrid, List<Ability> allAbilities)
	{
        ClearGrid(dynamicGrid);
        FillAbilitiesPanelAdd(dynamicGrid, allAbilities);
    }

    private void FillAbilitiesPanelAdd(DynamicGrid dynamicGrid, List<Ability> allAbilities)
    {
        if (!dynamicGrid)
        {
            return;
        }
        allAbilities.Sort();
        foreach (Ability ability in allAbilities)
        {
            AbilityButtonControl abilityButtonControl = Instantiate(abilityButtonPrefab).GetComponent<AbilityButtonControl>();
            abilityButtonControl.transform.SetParent(dynamicGrid.transform);
            abilityButtonControl.SetAbility(ability);
        }
        dynamicGrid.SetSize();
    }

    private void FillSelectUnitPanel(DynamicGrid dynamicGrid, List<Unit> listOfUnits)
    {
        ClearGrid(dynamicGrid);
        FillSelectUnitPanelAdd(dynamicGrid, listOfUnits);
    }

    private void FillSelectUnitPanelAdd(DynamicGrid dynamicGrid, List<Unit> listOfUnits)
    {
        if (!dynamicGrid)
        {
            return;
        }

        listOfUnits.Sort();
        SelectUnitButton selectUnitButton = Instantiate(selectUnitPrefab).GetComponent<SelectUnitButton>();
        selectUnitButton.transform.SetParent(dynamicGrid.transform);
        selectUnitButton.Assign(listOfUnits);
        dynamicGrid.SetSize();
    }

    private void ClearGrid(DynamicGrid dynamicGrid)
    {
        if (!dynamicGrid)
        {
            return;
        }

        if (dynamicGrid.transform.childCount > 0)
        {
            foreach (Transform child in dynamicGrid.transform)
            {
                child.gameObject.SetActive(false);
                Destroy(child.gameObject);
            }
        }
    }



    internal void UpdateAlienValues()
    {
        income.text = "Income: " + AlienTeam.Instance.income;
        upkeep.text = "Upkeep: " + AlienTeam.Instance.upkeep;
        energy.text = "Energy: " + AlienTeam.Instance.currentEnergy;
        bioFuel.text = "BioFuel: " + AlienTeam.Instance.currentBiofuel;
    }

    public void UpdateSelectionPanel(TeamStatus team)
	{


	}

    public void PopulatedAbilityInfo(Ability ability)
    {
        abilityTitleRef.text = ability.name;
        abilityDescriptionRef.text = ability.description;
        abilityTitleRef.gameObject.SetActive(true);
    }

    public void ClearAbilityInfo()
    {
        abilityTitleRef.gameObject.SetActive(false);
    }

    public void SelectSource()
    {
        PlayerInputHandler.Instance.SelectSingle(AlienTeam.Instance.theSource);
    }

    public void MainIconPressed()
    {
        if (mainSelectable)
        {
            PlayerInputHandler.Instance.AdjustCamera(mainSelectable);
        }

    }


    public void ToggleCorruptMode()
    {
        if (corruptionChoiceHandler.gameObject.activeSelf)
        {
            corruptionChoiceHandler.gameObject.SetActive(false);
            corruptClearAllPanel.gameObject.SetActive(false);
            corruptHandlerOutline.enabled  = false;
            PlayerInputHandler.Instance.TurnOffCorruptMode();
        }
        else
        {
            corruptionChoiceHandler.gameObject.SetActive(true);
            corruptClearAllPanel.gameObject.SetActive(true);
            corruptHandlerOutline.enabled = true;
            EventChunk eventChunk = new EventChunk();
            eventChunk.AddListener(ToggleCorruptionSingle);
            PlayerInputHandler.Instance.TurnOnCorruptMode(eventChunk, corruptionChoicePreferences);
        }

    }

    public void SetCanvasMode(CanvasHiveMode newMode)
    {
        SetCanvasMode(newMode, true);
    }

    public void SetCanvasMode(CanvasHiveMode newMode, bool clearAbilities)
    {

        PlayerInputHandler.Instance.ClearAbilities();
        foreach (DynamicGrid rect in hiveAbilityPanels)
        {
            rect.gameObject.SetActive(false);
        }

        switch (newMode)
        {
            case CanvasHiveMode.Unit:
                unitsPanel.gameObject.SetActive(true);
                break;
            case CanvasHiveMode.Build:
                buildingPanel.gameObject.SetActive(true);
                break;
            case CanvasHiveMode.Recruit:
                recruitmentPanel.gameObject.SetActive(true);
                break;
            case CanvasHiveMode.Items:
                floorItemsPanel.gameObject.SetActive(true);
                break;
            case CanvasHiveMode.Ship:
                shipPanel.gameObject.SetActive(true);
                break;
            default:
                break;
        }
        hiveButtonPanel.AdjustOutline(newMode);
    }

    public void UpdateHivePanels()
    {


        UpdateUnits();
        FillAbilitiesPanel(buildingPanel, AlienTeam.Instance.placeRoomAbilities.ConvertAll(x => (Ability)x));
        FillAbilitiesPanelAdd(buildingPanel, AlienTeam.Instance.spawnUnitAbilities.ConvertAll(x => (Ability)x));
    }

    public void UpdateUnits()
    {
        if (AlienTeam.Instance.allWorkers.Count > 0)
        {
            workerButton.Assign(AlienTeam.Instance.allWorkers.Cast<Unit>().ToList());
        }
        var myList = AlienTeam.Instance.allMyUnits.ToList();
        myList.Sort((pair1, pair2) => pair1.Value[0].CompareTo(pair2.Value[0]));
        //Fill unit panel with select unit ability.
        ClearGrid(unitsPanel);
        foreach (var key in myList)
        {
            FillSelectUnitPanelAdd(unitsPanel, key.Value);
        }

    }

    public void ChoiceCorruptAll()
    {
        HashSet<Chunk> allChunks = MapController.Instance.GetAllChunks();
        foreach (Chunk chunk in allChunks)
        {
            if (!chunk.IsCorruptionTurnedOn())
            {
                ToggleCorruption(chunk,true);
            }
        }
        MapController.Instance.ChunkHasChanged();
    }

    public void ChoiceClearAll()
    {
        HashSet<Chunk> allChunks = MapController.Instance.GetAllChunks();
        foreach (Chunk chunk in allChunks)
        {
            if (chunk.IsCorruptionTurnedOn())
            {
                ToggleCorruption(chunk,false);
            }
        }
        MapController.Instance.ChunkHasChanged();
    }


    public void LoadCorruptionHandlerTiles(HashSet<Chunk> positions)
    {
        corruptionChoiceHandler.LoadTiles(positions, corruptionHandlerSprite);
    }

    public void ToggleCorruptionSingle(Chunk chunk)
    {

        ToggleCorruption(chunk);
        MapController.Instance.ChunkHasChanged();
    }

    public void ToggleCorruption(Chunk chunk)
    {
        //Called through single or group.
        corruptionChoiceHandler.SetTile(chunk.SetCorruption(PlayerInputHandler.Instance.GetToggleCorruptMode()), chunk.MyCoords);

    }

    public void ToggleCorruption(Chunk chunk, bool set)
    {
        //Called through single or group.
        corruptionChoiceHandler.SetTile(chunk.SetCorruption(set), chunk.MyCoords);

    }

    public void AbilityHovering(string title, string description, string hotkey = "")
    {
        abilityTitleRef.transform.parent.gameObject.SetActive(true);
        abilityTitleRef.text = title;
        abilityDescriptionRef.text = description;
    }

    public void AbilityStoppedHovering()
    {
        abilityTitleRef.transform.parent.gameObject.SetActive(false);
    }
}

public enum CanvasHiveMode { Unit, Build, Recruit, Items, Ship }
