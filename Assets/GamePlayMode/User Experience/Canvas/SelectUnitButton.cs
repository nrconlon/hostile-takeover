﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class SelectUnitButton : MonoBehaviour {

    List<Unit> myUnits;
    [SerializeField] Text total;
    [SerializeField] Text available;

    private void Awake()
    {
        total.text = "0";
        available.text = "0";
    }

    public void Assign(List<Unit> units)
    {
        if (units.Count <= 0)
        {
            total.text = "0";
            available.text = "0";
        }
        else
        {
            total.text = units.Count.ToString();
            myUnits = units;
            GetComponent<Image>().sprite = units[0].icon;
            int count = 0;
            foreach (Unit unit in myUnits)
            {
                if (!SelectableObject.currentlySelected.Contains(unit))
                {
                    count++;
                }
            }
            available.text = count.ToString();
        }

    }

    public void OnClick()
    {
        //Select Unit, lower my number, re-sort list
        if(myUnits.Count > 0)
        {
            foreach(Unit unit in myUnits)
            {
                if (!SelectableObject.currentlySelected.Contains(unit))
                {
                    PlayerInputHandler.Instance.SelectSingle(unit);
                    return;
                }
            }
            PlayerInputHandler.Instance.SelectSingle(myUnits[0]);

        }

    }

    public void SelectAllClick()
    {
        PlayerInputHandler.Instance.SelectGroup(myUnits.Cast<SelectableObject>().ToList());
    }
}
