﻿using UnityEngine;
using UnityEngine.UI;

public class SelectedButtonControl : MonoBehaviour {
    bool isMain = false;
    SelectableObject mySelectable;
    [SerializeField] Outline myOutline;

    public void Assign(SelectableObject selectable)
    {
        mySelectable = selectable;
        GetComponent<Image>().sprite = selectable.icon;
    }

    public void OnClick()
    {
        if(isMain)
        {
            PlayerInputHandler.Instance.SelectSingle(mySelectable);
        }
        else
        {
            CanvasController.Instance.SetMain(mySelectable);
        }
    }

    public void SetToMain(bool setToMain)
    {
        isMain = setToMain;
        myOutline.enabled = setToMain;
    }

    public void SetToMain(SelectableObject selectable)
    {
        isMain = selectable == mySelectable;
        myOutline.enabled = selectable == mySelectable;

    }

}
