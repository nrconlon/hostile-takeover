﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemButtonHandler : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{

    public Sprite defaultSprite;
    public string hotKey;
    private InventoryItem myItem;

    GameObject abilityInfoPanel;

    private void Awake()
    {
        Close();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!myItem) return;
        if (eventData.button == PointerEventData.InputButton.Left)
            myItem.myAbility.Activate();
        else if (eventData.button == PointerEventData.InputButton.Right)
            PrepareItemDrop();
    }

    public void SetItem(InventoryItem item)
    {
        GetComponent<Image>().sprite = item.icon;
        this.myItem = item;
    }

    public void ClearItem()
    {
        GetComponent<Image>().sprite = defaultSprite;
        myItem = null;
    }

    public void Open()
    {
        GetComponent<Image>().color = Color.white;

    }

    public void Close()
    {
        GetComponent<Image>().color = Color.grey;

    }


    private void PrepareItemDrop()
    {
        EventFloorSelectable dropOrGiveItem = new EventFloorSelectable();
        dropOrGiveItem.AddListener(ItemDropOrGive);
        PlayerInputHandler.Instance.AbilityFloorSelectable(dropOrGiveItem, GamePlayMode.Instance.giveItemPreferences);

    }

    void ItemDropOrGive(SelectableObject target, Vector3 location)
    {
        if (target)
        {
            if (target.CanPickUpItem(myItem, false))
            {
                myItem.myHolder.GiveItem(myItem, target);
            }
        }
        else if (location != Vector3.zero)
        {
            myItem.myHolder.DropItem(myItem, location);
        }

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(myItem)
        {
            CanvasController.Instance.AbilityHovering(myItem.name, myItem.description, hotKey);
        }

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        CanvasController.Instance.AbilityStoppedHovering();
    }
}
