﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HiveButtonPanel : MonoBehaviour {
    [SerializeField] Outline units;
    [SerializeField] Outline build;
    [SerializeField] Outline recruit;
    [SerializeField] Outline items;
    [SerializeField] Outline ship;

    List<Outline> allMyOutlines;

    private void Start()
    {
        allMyOutlines = new List<Outline>()
        {
            units,build,recruit,items,ship
        };
        TurnOffAll();
        UnitsSelected();
    }

    private void TurnOffAll()
    {
        foreach (Outline outline in allMyOutlines)
        {
            outline.enabled = false;
        }
    }

    public void AdjustOutline(CanvasHiveMode newMode)
    {
        TurnOffAll();
        switch (newMode)
        {
            case CanvasHiveMode.Unit:
                units.enabled = true;
                break;
            case CanvasHiveMode.Build:
                build.enabled = true;
                break;
            case CanvasHiveMode.Recruit:
                recruit.enabled = true;
                break;
            case CanvasHiveMode.Items:
                items.enabled = true;
                break;
            case CanvasHiveMode.Ship:
                ship.enabled = true;
                break;
            default:
                break;
        }
    }





    public void UnitsSelected()
    {
        TurnOffAll();
        units.enabled = true;
        CanvasController.Instance.SetCanvasMode(CanvasHiveMode.Unit);

    }

    public void BuildSelected()
    {
        TurnOffAll();
        build.enabled = true;
        CanvasController.Instance.SetCanvasMode(CanvasHiveMode.Build);

    }

    public void RecruitSelected()
    {
        TurnOffAll();
        recruit.enabled = true;
        CanvasController.Instance.SetCanvasMode(CanvasHiveMode.Recruit);

    }
    public void ItemSelected()
    {
        TurnOffAll();
        items.enabled = true;
        CanvasController.Instance.SetCanvasMode(CanvasHiveMode.Items);

    }
    public void ShipSelected()
    {
        TurnOffAll();
        ship.enabled = true;
        CanvasController.Instance.SetCanvasMode(CanvasHiveMode.Ship);

    }


}
