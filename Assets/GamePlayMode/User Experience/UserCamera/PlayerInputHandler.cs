﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

public class PlayerInputHandler : MonoBehaviour {
	private Camera myCamera;

	float maxRaycastDepth = 100f; // Hard coded value
    TeamStatus statusLastFrame;

	[SerializeField] Texture2D mainCursor = null;
    [SerializeField] Texture2D mainCursorEnemy = null;
    [SerializeField] Texture2D mainCursorFriendly = null;
    [SerializeField] Texture2D mainCursorNeutral = null;
    [SerializeField] Texture2D targetCursor = null;
    [SerializeField] Texture2D targetCursorEnemy = null;
    [SerializeField] Texture2D targetCursorFriendly = null;
    [SerializeField] Texture2D targetCursorNeutral = null;

	[SerializeField] Vector2 cursorMainSpot = new Vector2(0, 0);
    [SerializeField] Vector2 cursorTargetSpot = new Vector2(50, 50);



    [SerializeField] Sprite selectionCircleFriendly;
    [SerializeField] Sprite selectionCircleNeutral;
    [SerializeField] Sprite selectionCircleEnemy;


    [SerializeField] SpriteRenderer buildingSpriteRenderer;
    SpriteRenderer holdingOfBuildingSpriteRenderer;

    EventFloor currentEventFloor = null;
    EventFloorSelectable currentEventFloorSelectable = null;
    EventSelectable currentEventSelectable = null;
    EventChunk currentEventChunk = null;
    SelectPreferences currentSelectPreferences = null;
    ChunkPreferences currentChunkPreferences = null;

    bool usingAbility = false;
    bool usingAbilityLastFrame;

    Color cursorColorGreen = new Color(1, 1, 1, 0.5f);
    Color cursorColorRed = new Color(1, 0, 0, 0.5f);

    InputMode currentInputMode = InputMode.normalMode;
    HashSet<Chunk> alreadyUsedChunks = new HashSet<Chunk>();
    SelectableObject lastTargetedObject;
    Vector3 lastTargetLocation;
    bool corruptionMode = false;
    bool ToggleCorruptionMode = false;
    bool rightClickCancelledAbility = false;
    List<Unit> movingUnits = new List<Unit>();

    public static PlayerInputHandler Instance { get; private set; }


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            SelectableObject.selectionCircleFriendly = selectionCircleFriendly;
            SelectableObject.selectionCircleNeutral = selectionCircleNeutral;
            SelectableObject.selectionCircleEnemy = selectionCircleEnemy;

            myCamera = GetComponent<Camera>();
        }


    }

	// Update is called once per frame
	void Update () {
		RayCastCheck();


		if (Input.GetKeyDown("q"))
		{
			//select hivemind
		}
        if (Input.GetKeyDown("c"))
        {
            CanvasController.Instance.ToggleCorruptMode();
        }
    }

    public InputMode GetCurrentInputMode()
    {
        return currentInputMode;
    }



    private void RayCastCheck()
    {
        //Didn't hit UI, cursor over something
        Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = mousePosition
        };
        List<RaycastResult> tempRaycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, tempRaycastResults);
        if (tempRaycastResults.Count != 1) //Ignore drag handler and selection box.
        {
            AdjustCursor(TeamStatus.None);
            return;
        }
        else
        {
            Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit[] rayCastHits;
            rayCastHits = Physics.RaycastAll(ray, maxRaycastDepth);
            //Sprite placement active?
            SelectableObject selectableObject = GetSelectableObject(rayCastHits);  //Could return null
            //Cursor stuff for the placement sprite
            if (currentEventChunk != null && buildingSpriteRenderer.sprite)
            {
                Chunk chunk = GetChunk(rayCastHits);
                if (chunk)
                {

                    Vector3 chunkPosition = chunk.transform.position;
                    buildingSpriteRenderer.transform.position = new Vector3(chunkPosition.x, chunkPosition.y + 1, chunkPosition.z);
                    //Check if it can be placed, if not show red sprite.  Is unit in the way?  How the heck can I know that?
                    if (currentChunkPreferences.FitsPreferences(chunk))
                    {
                        buildingSpriteRenderer.color = cursorColorGreen;
                    }
                    else
                    {
                        buildingSpriteRenderer.color = cursorColorRed;
                    }
                }

            }
            else
            {
                if (selectableObject)
                {
                    if (usingAbility && currentSelectPreferences != null)
                    {
                        if (currentSelectPreferences.FitsPreferences(selectableObject))
                        {
                            AdjustCursor(selectableObject.GetTeam());
                        }
                        else
                        {
                            AdjustCursor(TeamStatus.None);
                        }
                    }
                    else
                    {
                        AdjustCursor(selectableObject.GetTeam());
                    }
                }
                else
                {
                    AdjustCursor(TeamStatus.None);
                }
            }
            //Change cursor if selectable is enemy.  Grab selectable if clicking.


            if (Input.GetMouseButton(0) && currentInputMode == InputMode.Persist)
            {
                //This is for click and slide corruption toggleing
                Chunk chunk = GetChunk(rayCastHits);
                if(chunk)
                {
                    if (alreadyUsedChunks.Count < 1 && corruptionMode)
                    {
                        ToggleCorruptionMode = !chunk.IsCorruptionTurnedOn();
                    }
                    if (!alreadyUsedChunks.Contains(chunk))
                    {
                        OnLeftClick(rayCastHits, selectableObject);
                        alreadyUsedChunks.Add(chunk);
                    }

                }

            }

            if (Input.GetMouseButtonUp(0))
            {
                if (currentInputMode != InputMode.Persist)
                {
                    OnLeftClick(rayCastHits, selectableObject);
                }

                alreadyUsedChunks.Clear(); //Clear corruption toggle check
                return;
            }

            if (Input.GetMouseButtonDown(1)) // Called on update.
            {
                rightClickCancelledAbility = OnRightClick(rayCastHits, selectableObject);
            }

            if (Input.GetMouseButton(1) && !rightClickCancelledAbility) // Called on update.
            {
                OnRightClick(rayCastHits, selectableObject);
            }

        }
    }

    public bool IsUsingAbility()
    {
        return usingAbility;
    }


    private void OnLeftClick(RaycastHit[] rayCastHits,SelectableObject hoveredSelectable)
    {
        //Check if using ability like building.  If not return false so that it can go on to select.
        if (usingAbility)
        {
            if (currentEventSelectable != null)
            {
                if (hoveredSelectable && currentSelectPreferences.FitsPreferences(hoveredSelectable))
                {
                    currentEventSelectable.Invoke(hoveredSelectable);
                    UsedAbility();
                    return;
                }
                else
                {
                    Debug.Log(currentSelectPreferences.failMessage);
                }
            }
            else if (currentEventFloorSelectable != null)
            {
                if (hoveredSelectable && currentSelectPreferences.FitsPreferences(hoveredSelectable))
                {
                    currentEventFloorSelectable.Invoke(hoveredSelectable,Vector3.zero);
                    UsedAbility();
                    return;
                }
                else
                {
                    Vector3 moveLocation = GetMoveLocation(rayCastHits);
                    if(moveLocation != Vector3.zero)
                    {
                        currentEventFloorSelectable.Invoke(null, moveLocation);
                        UsedAbility();
                        return;
                    }
                    else
                    {
                        Debug.Log(currentSelectPreferences.failMessage);
                    }
                }
            }
            else if (currentEventFloor != null)
            {
                Vector3 moveLocation = GetMoveLocation(rayCastHits);
                if (moveLocation != Vector3.zero)
                {
                    currentEventFloor.Invoke(moveLocation);
                    UsedAbility();
                    return;
                }
                else
                {
                    Debug.Log("Invalid Target");
                }
            }
            else if (currentEventChunk != null)
            {
                Chunk chunk = GetChunk(rayCastHits);
                if (chunk && currentChunkPreferences.FitsPreferences(chunk))
                {
                    if(buildingSpriteRenderer.sprite)
                    {
                        CreateBuildingSpriteHolder();
                    }
                    currentEventChunk.Invoke(chunk);
                    UsedAbility();
                    return;
                }
                else
                {
                    //Hovering over blank
                }
            }
            else
            {
                SetUsingAbility(false);
            }
        }
        else if(hoveredSelectable)
        {
            SelectSingle(hoveredSelectable);
        }
        else
        {
            //click didint work.  No worries.
        }
    }


    private bool OnRightClick(RaycastHit[] rayCastHits, SelectableObject hoveringSelectableObject)
    {
        if (usingAbility)
        {
            if (corruptionMode)
            {
                CanvasController.Instance.ToggleCorruptMode();
            }
            else
            {
                ClearAbilities();

            }
            return true;

        }
        else
        {
            //Try for when the list has no units
            try
            {
                //Looping through every selectable.  If we find a selectable then set the mouse point as the location, if found a collection then set that one as the location.
                Vector3 moveLocation = GetMoveLocation(rayCastHits);
                foreach (SelectableObject selectableObject in SelectableObject.currentlySelected)
                {
                    if (selectableObject.GetTeam() == TeamStatus.Human) continue;
                    if (hoveringSelectableObject)
                    {
                        if (lastTargetedObject != hoveringSelectableObject)
                        {
                            selectableObject.RightClickedSelectable(hoveringSelectableObject);
                            lastTargetedObject = hoveringSelectableObject;
                            moveLocation = Vector3.zero;
                        }

                    }
                    else if (moveLocation != Vector3.zero && Vector3.Distance(lastTargetLocation, moveLocation) > 0.2f)
                    {
                        if(selectableObject is Unit)
                        {
                            movingUnits.Add((Unit)selectableObject);
                        }
                        else
                        {
                            selectableObject.RightClickedFloor(moveLocation);
                        }
                    }
                    else
                    {
                        //clicked space?
                    }
                }

                if(movingUnits.Count > 0)
                {
                    MoveUnitsInFormation(movingUnits, moveLocation);
                    lastTargetLocation = moveLocation;
                    lastTargetedObject = null;
                    movingUnits.Clear();
                }
            }
            catch { }

        }
        return false;

    }

    private void MoveUnitsInFormation(List<Unit> movingUnits, Vector3 location)
    {
        float count = 0;
        //Can make this advanced later
        foreach(Unit unit in movingUnits)
        {
            unit.RightClickedFloor(location, count / 20f);
            count++;
        }

    }

    public void UpdateAbilitiesAndInfoPanels()
    {
        CanvasController.Instance.UpdateAbilitiesAndInfoPanels();
    }


    public void TurnOffCorruptMode()
    {
        if(corruptionMode)
        {
            corruptionMode = false;
            ClearAbilities();
        }
    }

    public void TurnOnCorruptMode(EventChunk eventChunk, ChunkPreferences preferences)
    {
        if (!corruptionMode)
        {
            AbilityChunk(eventChunk, preferences, null, InputMode.Persist);
            corruptionMode = true;
        }
    }


    public void SelectGroup(List<SelectableObject> prioritySelectables)
    {
        if(!usingAbility)
        {
            SelectableObject.SelectGroup(prioritySelectables);
            UpdateAbilitiesAndInfoPanels();
        }

    }

    public void AdjustCamera(SelectableObject selectableObject)
    {
        Vector3 position = selectableObject.transform.position;
        transform.position = new Vector3(position.x, transform.position.y, position.z);
    }

    public void SelectSingle(SelectableObject prioritySelectable)
    {
        if (!usingAbility)
        {
            if (SelectableObject.currentlySelected.Count == 1 && SelectableObject.currentlySelected[0] == prioritySelectable)
            {
                if(HoldingShift())
                {
                    prioritySelectable.OnDeselect();
                }
                else
                {
                    AdjustCamera(prioritySelectable);
                }

            }
            else
            {
                prioritySelectable.OnSelectSingle();
                UpdateAbilitiesAndInfoPanels();
            }
        }


    }

    public void SelectSingleAdd(SelectableObject prioritySelectable)
    {
        prioritySelectable.OnSelect();
        UpdateAbilitiesAndInfoPanels();

    }


    public void AbilityFloorSelectable(EventFloorSelectable eventFloorSelectable, SelectPreferences preferences)
    {
        ClearAbilities();
        currentEventFloorSelectable = eventFloorSelectable;
        currentSelectPreferences = preferences;
        SetUsingAbility(true);

    }
    public void AbilityFloor(EventFloor eventFloor)
    {
        ClearAbilities();
        currentEventFloor = eventFloor;
        SetUsingAbility(true);
    }
    public void AbilitySelectable(EventSelectable eventSelectable, SelectPreferences preferences)
    {
        ClearAbilities();
        currentEventSelectable = eventSelectable;
        currentSelectPreferences = preferences;
        SetUsingAbility(true);
    }
    public void AbilityChunk(EventChunk eventChunk, ChunkPreferences preferences, Sprite sprite, InputMode inputMode = InputMode.normalMode)
    {
        ClearAbilities();
        currentEventChunk = eventChunk;
        currentChunkPreferences = preferences;
        SetUsingAbility(true);
        if (sprite)
        {
            buildingSpriteRenderer.gameObject.SetActive(true);
            buildingSpriteRenderer.sprite = sprite;
            Cursor.visible = false;
        }
        currentInputMode = inputMode;
    }

    public void ClearAbilities()
    {
        if(corruptionMode)
        {
            CanvasController.Instance.ToggleCorruptMode();  //Loops back to here but with corruption mode off.
            return;
        }

        currentInputMode = InputMode.normalMode;
        TurnOffSpriteRenderer();
        currentEventFloor = null;
        currentEventFloorSelectable = null;
        currentEventSelectable = null;
        currentEventChunk = null;
        currentSelectPreferences = null;
        currentChunkPreferences = null;
        SetUsingAbility(false);
    }

    private void SetUsingAbility(bool set)
    {
        usingAbility = set;
    }


    private void CreateBuildingSpriteHolder()
    {
        holdingOfBuildingSpriteRenderer = Instantiate(buildingSpriteRenderer.gameObject).GetComponent<SpriteRenderer>();
    }

    public SpriteRenderer GetBuildingSpriteRenderer()
    {
        return holdingOfBuildingSpriteRenderer;
    }

    private void UsedAbility()
    {
        if (corruptionMode)
        {
            return;
        }

        if (currentInputMode == InputMode.optionalPersist && (HoldingShift()))
        {
            return;
        }

        if(currentInputMode == InputMode.Persist)
        {
            return;
        }
        ClearAbilities();
    }

    bool HoldingShift()
    {
        return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift); 
    }



    private void TurnOffSpriteRenderer()
    {
        buildingSpriteRenderer.gameObject.SetActive(false);
        buildingSpriteRenderer.sprite = null;

    }

    public bool GetToggleCorruptMode()
    {
        return ToggleCorruptionMode;
    }



    private SelectableObject GetSelectableObject(RaycastHit[] rayCastHits)
    {

        foreach (RaycastHit RaycastHit in rayCastHits)
        {
            GameObject hitObject = RaycastHit.transform.gameObject;
            SelectableObject selectableResult = hitObject.GetComponent<SelectableObject>();
            if (selectableResult)
            {
                return selectableResult;  
            }
        }
        return null;
    }

    private Vector3 GetMoveLocation(RaycastHit[] rayCastHits)
    {
        foreach (RaycastHit RaycastHit in rayCastHits)
        {
            GameObject hitObject = RaycastHit.transform.gameObject;
            Chunk chunk = hitObject.GetComponent<Chunk>();
            if (chunk)
            {
                return RaycastHit.point;
            }
        }
        return Vector3.zero;
    }

    private Chunk GetChunk(RaycastHit[] rayCastHits)
    {
        foreach (RaycastHit RaycastHit in rayCastHits)
        {
            GameObject hitObject = RaycastHit.transform.gameObject;
            Chunk chunk = hitObject.GetComponent<Chunk>();
            if (chunk)
            {
                return chunk;
            }
        }
        return null;
    }



    void AdjustCursor(TeamStatus currentSatus)
    {
        if (buildingSpriteRenderer.sprite) return;
        Cursor.visible = true;
        if (usingAbility != usingAbilityLastFrame || currentSatus != statusLastFrame)
        {
            usingAbilityLastFrame = usingAbility;
            statusLastFrame = currentSatus;
            if (usingAbility)
            {
                switch (currentSatus)
                {
                    case TeamStatus.Alien:
                        SetCursor(targetCursorFriendly, cursorTargetSpot);
                        break;
                    case TeamStatus.Neutral:
                        SetCursor(targetCursorNeutral, cursorTargetSpot);
                        break;
                    case TeamStatus.Human:
                        SetCursor(targetCursorEnemy, cursorTargetSpot);
                        break;
                    case TeamStatus.None:
                        SetCursor(targetCursor,cursorTargetSpot);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (currentSatus)
                {
                    case TeamStatus.Alien:
                        SetCursor(mainCursorFriendly, cursorMainSpot);
                        break;
                    case TeamStatus.Neutral:
                        SetCursor(mainCursorNeutral, cursorMainSpot);
                        break;
                    case TeamStatus.Human:
                        SetCursor(mainCursorEnemy, cursorMainSpot);
                        break;
                    case TeamStatus.None:
                        SetCursor(mainCursor, cursorMainSpot);
                        break;
                    default:
                        break;
                }
            }
        }

    }


    private void SetCursor(Texture2D cursor, Vector2 position)
	{
		if (cursor)
		{
            Cursor.SetCursor(cursor, position, CursorMode.Auto);
		}
		else
		{

            Cursor.SetCursor(mainCursor, new Vector2(0,0), CursorMode.Auto);
		}
	}



}

public enum InputMode { normalMode, Persist, optionalPersist}

