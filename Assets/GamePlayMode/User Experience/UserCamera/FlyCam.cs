﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FlyCam : MonoBehaviour
{

    [SerializeField] float mainSpeed = 200; //regular speed
    [SerializeField] float shiftAdd = 2000; //multiplied by how long shift is held.  Basically running
    [SerializeField] float maxVelocityChange = 100.0f;
    [SerializeField] float mouseScrollHeight = 5;
    [SerializeField] float mouseScrollSpeed = 20;
    [SerializeField] float minCameraHeight = 3;
    [SerializeField] float maxCameraHeight = 70;
    [SerializeField] float startingCameraHeight = 25;
    [SerializeField] float orthoSizeMultiplier = 2f;
    [SerializeField] float Boundary = 50f;

    float currentSpeed;
    Rigidbody m_RigidBody;
    Camera myCamera;

    private float desiredOrthoSize;
    private float desiredHeight;

    private int theScreenWidth;
    private int theScreenHeight;
    bool inOrthographic = true;


    private void Start()
    {
        Vector3 angles = transform.eulerAngles;
        m_RigidBody = GetComponent<Rigidbody>();
        myCamera = GetComponent<Camera>();

        mouseScrollHeight /= orthoSizeMultiplier;
        startingCameraHeight /= orthoSizeMultiplier;
        minCameraHeight /= orthoSizeMultiplier;
        maxCameraHeight /= orthoSizeMultiplier;

        desiredOrthoSize = startingCameraHeight;
        desiredHeight = startingCameraHeight;
        inOrthographic = myCamera.orthographic;

        theScreenWidth = Screen.width;
        theScreenHeight = Screen.height;


    }

    private void FixedUpdate()
    {
        currentSpeed = mainSpeed;


        //Keyboard commands

        //if (Input.GetMouseButton(1))
        //{
        //	HandleChangeRotation();
        //}

        HandleMouseWheelZoom();

        if (Input.GetKey(KeyCode.LeftShift))
        {
            currentSpeed = currentSpeed + shiftAdd;
        }
        if (inOrthographic)
        {
            currentSpeed = currentSpeed * 0.02f * myCamera.orthographicSize;
        }
        else
        {
            currentSpeed = currentSpeed * 0.02f * transform.position.y;
        }

        //z is up down, x is left right
        //if(Input.GetKeyDown("w"))
        //{
        //	Vector3 oldPosition = transform.position;
        //	transform.position.Set(oldPosition.x, oldPosition.y, oldPosition.z + currentSpeed);
        //}

        //Vector3 direction = transform.rotation * Vector3.forward;
        CheckForMovement();


        
    }

    void CheckForMovement()
    {
        

        float hor = Input.GetAxisRaw("Horizontal");
        float ver = Input.GetAxisRaw("Vertical");

        if (Input.mousePosition.x > theScreenWidth - Boundary && Input.mousePosition.x < theScreenWidth + Boundary * 3)
        {
            hor += 1; // move on +X axis
        }
        if (Input.mousePosition.x < 0 + Boundary && Input.mousePosition.x > 0 - Boundary * 3)
        {
            hor -= 1; // move on -X axis
        }
        if (Input.mousePosition.y > theScreenHeight - Boundary && Input.mousePosition.y < theScreenHeight + Boundary * 3)
        {
            ver += 1; // move on +Z axis
        }
        if (Input.mousePosition.y < 0 + Boundary && Input.mousePosition.y > 0 - Boundary * 3)
        {
            ver -= 1; // move on -Z axis
        }
        Vector3 targetDirection = new Vector3(hor, 0, ver);


        Vector3 targetVelocity = targetDirection;
        targetVelocity *= currentSpeed;

        Vector3 velocityChange = (targetVelocity - m_RigidBody.velocity);
        velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
        velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
        velocityChange.y = 0;
        m_RigidBody.AddForce(velocityChange, ForceMode.VelocityChange);
    }


//private void HandleChangeRotation()
//{
//	x += Input.GetAxisRaw("Mouse X") * mouseSensitivity * Time.deltaTime;
//	y -= Input.GetAxisRaw("Mouse Y") * mouseSensitivity * Time.deltaTime;
//	Quaternion rotation = Quaternion.Euler(y, x, 0);
//	transform.rotation = rotation;
//}

    private void HandleMouseWheelZoom()
    {
        float changeY = Input.GetAxis("Mouse ScrollWheel");
        if (changeY != 0)
        {
            Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
            {
                position = mousePosition
            };
            List<RaycastResult> tempRaycastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, tempRaycastResults);
            if (tempRaycastResults.Count > 2) //Ignore drag handler and selection box.
            {
                changeY = 0;
            }
        }
        if (inOrthographic)
        {
            if (changeY != 0)
            {
                desiredOrthoSize = Mathf.Clamp((desiredOrthoSize - (changeY * mouseScrollHeight)), minCameraHeight, maxCameraHeight);
            }
            float currentOrthoSize = myCamera.orthographicSize;

            float speedToMove = mouseScrollSpeed * Time.deltaTime;

            if (currentOrthoSize > desiredOrthoSize)
            {
                if ((currentOrthoSize - desiredOrthoSize) < speedToMove)
                {
                    myCamera.orthographicSize = desiredOrthoSize;
                }
                else
                {
                    myCamera.orthographicSize = currentOrthoSize - speedToMove;
                }
            }
            else if (currentOrthoSize < desiredOrthoSize)
            {
                if ((desiredOrthoSize - currentOrthoSize) < speedToMove)
                {
                    myCamera.orthographicSize = desiredOrthoSize;
                }
                else
                {
                    myCamera.orthographicSize = currentOrthoSize + speedToMove;
                }
            }
        }
        else
        {
            if (changeY != 0)
            {
                desiredHeight = Mathf.Clamp((desiredHeight - (changeY * mouseScrollHeight)), minCameraHeight, maxCameraHeight);
            }
            float currentHeight = transform.position.y;
            float speedToMove = mouseScrollSpeed * Time.deltaTime;

            if (currentHeight > desiredHeight)
            {
                if ((currentHeight - desiredHeight) < speedToMove)
                {
                    transform.position = new Vector3(transform.position.x, desiredHeight, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(transform.position.x, currentHeight - speedToMove, transform.position.z);
                }
            }
            else if (currentHeight < desiredHeight)
            {
                if ((desiredHeight - currentHeight) < speedToMove)
                {
                    transform.position = new Vector3(transform.position.x, desiredHeight, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(transform.position.x, currentHeight + speedToMove, transform.position.z);
                }
            }
        }
    }

    public void SetCamPosition(Vector2 position)
    {
        transform.position = new Vector3(position.x, transform.position.y, position.y);
    }
       
		

}