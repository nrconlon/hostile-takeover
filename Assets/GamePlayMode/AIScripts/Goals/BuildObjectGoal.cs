﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwordGC.AI.Goap;

public class BuildObjectGoal : GoapGoal
{
    GameObject building;
    FloorItem assosiatedItem; // could be null

    public BuildObjectGoal(string key, GameObject building, FloorItem assosiatedItem, float multiplier = 1) : base(key, multiplier)
    {
        this.building = building;
        this.assosiatedItem = assosiatedItem;
    }
}
