﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwordGC.AI.Actions;
using SwordGC.AI.Goap;
using UnityEngine.AI;

public class UnitAIActionController : GoapAgent
{
    public override void Awake()
    {

        base.Awake();
        dataSet.SetData(GoapAction.Effects.HAS_OBJECT, false);
    }

    public void StartBuildObject(GameObject building, Vector3 location)
    {
        goals.Add(GoapGoal.Goals.BUILD_BUILDING + building.name, new BuildObjectGoal("Build" + building.name, building,null,1));
        possibleActions.Add(new BuildAction(this, building, location));
    }
}
