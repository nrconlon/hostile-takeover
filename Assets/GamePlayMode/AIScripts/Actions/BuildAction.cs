﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwordGC.AI.Goap;

public class BuildAction : GoapAction {
    GameObject myBuilding;
    public BuildAction(GoapAgent agent, GameObject building, Vector3 buildLocation, float delay = 1) : base(agent, delay)
    {
        goal = GoapGoal.Goals.BUILD_BUILDING + building.name;
        myAgent = agent;
        position = buildLocation;
        effects.Add(Effects.BUILT_OBJECT, true);
        requiredRange = 1f;
        cost = 1;
        myBuilding = building;

    }


    public override void Perform()
    {
        GamePlayMode.Instance.SpawnSelectable(myBuilding, position);
        //Remove Item, which in turn removes this action.  For now remove it from here
        myAgent.goals.Remove(goal);
        myAgent.RemoveAction(this);
    }

}
