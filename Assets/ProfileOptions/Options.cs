﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "EditorObjects/Options/Options")]
public class Options : ScriptableObject {


    public bool fullScreen = false;
    public bool mute = false;
    public bool music = false;

}
