﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "EditorObjects/List/EvolveSkillList")]
public class EvolveSkillList : RuntimeList<EvolveSkill>
{
}

