﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvolveSkill : ScriptableObject  {

    public Sprite icon;
    public int evolutionPointCost;
    public new string name;
    public string description;


}


